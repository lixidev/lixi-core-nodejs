let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện restful api',
    extensions: [
        require('./server/_create'),
        require('./endpoint/_define'),
        require('./middleware/_define'),
    ],
})
