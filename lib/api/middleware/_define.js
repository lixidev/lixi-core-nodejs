let { Core, RequireModule } = require('../../../package')
let { Instance, MakeKey } = Core
let Joi = RequireModule('joi')
let Callsite = require('callsite')

module.exports = Instance.registerMethod({
    description: 'tạo api middleware',
    key: 'defineMiddleware',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi
                .string()
                .regex(/^[a-z_0-9.]+$/)
                .required()
                .description('tên middleware'),
            description: joi.string().description('mô tả về middleware'),
            option: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .description('các tham số cấu hình của middleware'),
            setup: joi
                .func()
                .maxArity(1)
                .description(
                    'hàm cài đặt end-point khi middleware được kích hoạt'
                ),
            handler: joi
                .func()
                .maxArity(1)
                .description(
                    'hàm xử lý khi middleware được gọi trong workflow'
                ),
        }),
    handler: function ({ enable, name, description, option, setup, handler }) {
        // #region [tạo instance & option]

        option = Joi.object(option)
        option = option.default(option.validate().value)

        let middleware = { enable, name, description, option, setup, handler }
        middleware.key = MakeKey({ text: name })
        middleware.file_path = Callsite()[2].getFileName()

        // #endregion

        return Instance.create({
            initial: middleware,
            extensions: [require('./load')],
        })
    },
})
