let { RequireModule } = require('../../../../package')
let Api = RequireModule('api')

module.exports = Api.defineMiddleware({
    name: 'error_handler',
    description: 'xử lý lỗi',
    handler: async function ({ service, api_server, context, exception }) {
        // #region [tạo thông tin về lỗi]

        let error = {
            code: exception.code || 'exception',
            message: exception.base_message || 'exception server error',
            data: exception.data,
        }

        let err = api_server.errors.find((x) => x.code === error.code)

        if (exception.message) error.message = exception.message
        else if (err && err.message) error.message = err.message

        if (api_server.option.show_error_stack) error.stack = exception.stack

        context.error = error

        // #endregion

        // #region [xác định http status code]

        context.http_status_code = 500
        if (exception.http_code) context.http_status_code = exception.http_code
        else if (err && err.http_code) context.http_status_code = err.http_code

        // #endregion

        // #region [ghi nhận log]

        if (service.option.show_log_on_error) {
            if (error.code === 'exception') service.logger.error(error)
            else service.logger.warn(error)
        }

        // #endregion
    },
})
