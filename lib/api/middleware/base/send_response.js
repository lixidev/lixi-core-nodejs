let { RequireModule } = require('../../../../package')
let Api = RequireModule('api')
let Moment = RequireModule('moment')

module.exports = Api.defineMiddleware({
    name: 'send_response',
    description: 'thực hiện xử lý response',
    handler: async function ({
        service,
        api_server,
        request,
        response,
        api,
        context,
    }) {
        context.meta.execution_time =
            context.meta.response_time - context.meta.request_time

        if (context.error) {
            await sendErrorResponse({
                service,
                api_server,
                request,
                response,
                api,
                context,
            })
        } else {
            await sendSuccessResponse({
                service,
                api_server,
                request,
                response,
                api,
                context,
            })
        }
    },
})

async function sendErrorResponse({
    service,
    api_server,
    request,
    response,
    api,
    context,
}) {
    let { meta_field, error_field } = getResponseOption({ api })

    // #region [chuẩn bị meta]

    let meta = {
        request_uuid: context.meta.request_uuid,
        request_time: Moment(context.meta.request_time).valueOf(),
        response_time: Moment(context.meta.response_time).valueOf(),
        execution_time: context.meta.execution_time,
        success: false,
    }

    if (api && api.error_meta_handler) {
        meta = await api.error_meta_handler({
            service,
            api_server,
            request,
            response,
            api,
            context,
            request_uuid: context.request_uuid,
            meta: meta,
            header: context.header,
            args: context.args,
        })
    }

    // #endregion

    // #region [chuẩn bị error]

    let error = context.error
    if (api && api.error_handler) {
        error = await api.error_handler({
            service,
            api_server,
            request,
            response,
            api,
            request_uuid: context.request_uuid,
            meta: meta,
            args: context.args,
            error,
        })
    }

    // #endregion

    response.status = context.http_status_code
    response.body = {
        [meta_field]: meta,
        [error_field]: error,
    }
}

async function sendSuccessResponse({
    service,
    api_server,
    request,
    response,
    api,
    context,
}) {
    let { meta_field, data_field } = getResponseOption({ api })

    // #region [chuẩn bị meta]

    meta = Object.assign({}, context.meta, {
        request_uuid: context.meta.request_uuid,
        request_time: Moment(context.meta.request_time).valueOf(),
        response_time: Moment(context.meta.response_time).valueOf(),
        execution_time: context.meta.execution_time,
        success: true,
    })

    if (api.success_meta_handler) {
        meta = await api.success_meta_handler({
            service,
            api_server,
            request,
            response,
            api,
            context,
            request_uuid: context.request_uuid,
            meta: meta,
            header: context.header,
            args: context.args,
        })
    }

    // #endregion

    response.status = 200
    response.body = {
        [meta_field]: meta,
        [data_field]: context.result !== undefined ? context.result : true,
    }
}

function getResponseOption({ api }) {
    let meta_field = 'meta'
    let data_field = 'data'
    let error_field = 'error'

    if (api) {
        let prepare = api.workflow.find((x) => x.name === 'prepare')
        if (prepare && prepare.option) {
            if (prepare.option.meta_field) {
                meta_field = prepare.option.meta_field
            }
            if (prepare.option.data_field) {
                data_field = prepare.option.data_field
            }
            if (prepare.option.error_field) {
                error_field = prepare.option.error_field
            }
        }
    }

    return { meta_field, data_field, error_field }
}
