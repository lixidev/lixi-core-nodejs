let { RequireModule } = require('../../../../package')
let { BaseError } = RequireModule('err')
let Api = RequireModule('api')
let Joi = RequireModule('joi')

module.exports = Api.defineMiddleware({
    name: 'paging',
    description: 'xử lý phân trang',
    setup: async function ({ api }) {
        api.parameter.page = Joi.number()
            .integer()
            .default(1)
            .min(1)
            .description('vị trí trang dữ liệu')
        api.parameter.page_size = Joi.number()
            .integer()
            .default(10)
            .min(1)
            .max(1000)
            .description('kích thước trang dữ liệu')
    },
    handler: async function ({ api_server, context }) {
        try {
            context.result = Joi.tryValidate({
                data: context.result,
                schema: Joi.object({
                    total: Joi.number().required().min(0),
                    items: Joi.array().required().items(Joi.any()),
                }),
            })
        } catch (e) {
            throw new BaseError({
                code: 'bad_response',
                message:
                    'paging result is invalid, must be object type with "total" and "items" field',
            })
        }

        context.meta.total = context.result.total
        context.meta.page = context.args.page
        context.meta.page_size = context.args.page_size
        context.meta.page_count = Math.ceil(
            context.meta.total / context.meta.page_size
        )
        context.meta.can_next =
            context.meta.page_count > 0 &&
            context.meta.page < context.meta.page_count
        context.meta.can_prev =
            context.meta.page_count > 0 && context.meta.page > 1

        context.result = context.result.items
    },
})
