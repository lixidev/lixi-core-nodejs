let { RequireModule } = require('../../../../package')
let Api = RequireModule('api')

module.exports = Api.defineMiddleware({
    name: 'run_handler',
    description: 'thực hiện xử lý request',
    handler: async function ({
        ctx,
        service,
        api_server,
        request,
        response,
        api,
        context,
    }) {
        if (api.handler) {
            context.result = await api.handler({
                ctx,
                service,
                _module: api._module,
                api_server,
                request,
                response,
                api,
                context,
                request_uuid: context.request_uuid,
                meta: context.meta,
                header: context.header,
                args: context.args,
                files: context.files,
            })
        }
    },
})
