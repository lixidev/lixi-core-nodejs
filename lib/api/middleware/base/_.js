module.exports = async function ({ api_server }) {
    // #region [push middleware]

    await require('./error_handler').load({
        api_server,
        default_value: { enable: true },
    })

    await require('./prepare').load({
        api_server,
        default_value: { enable: true },
    })

    await require('./parse_parameter').load({
        api_server,
        default_value: { enable: true },
    })

    await require('./file_upload').load({
        api_server,
        default_value: { enable: false },
    })

    await require('./run_handler').load({
        api_server,
        default_value: { enable: true },
    })

    await require('./paging').load({
        api_server,
        default_value: { enable: false },
    })

    await require('./parse_response').load({
        api_server,
        default_value: { enable: true },
    })

    await require('./send_response').load({
        api_server,
        default_value: { enable: true },
    })

    // #endregion

    // #region [set workflow]

    api_server.setWorkflow({
        workflow: [
            'prepare',
            'parse_parameter',
            'file_upload',
            'run_handler',
            'paging',
            'parse_response',
        ],
    })

    // #endregion
}
