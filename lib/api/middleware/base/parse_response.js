let { RequireModule } = require('../../../../package')
let { BaseError } = RequireModule('err')
let Api = RequireModule('api')
let Joi = RequireModule('joi')

module.exports = Api.defineMiddleware({
    name: 'parse_response',
    description: 'thực hiện xử lý response',
    handler: async function ({ api, context }) {
        try {
            if (api.response) {
                context.result = Joi.tryValidate({
                    data: context.result,
                    schema: api.response,
                })
            } else {
                context.result = undefined
            }
        } catch (e) {
            throw new BaseError({ code: 'bad_response' })
        }
    },
})
