let { RequireModule, Core } = require('../../../../package')
let { Location } = Core
let { BaseError, HttpError } = RequireModule('err')
let Api = RequireModule('api')
let Joi = RequireModule('joi')

module.exports = Api.defineMiddleware({
    name: 'parse_parameter',
    description: 'xử lý tham số đầu vào',
    handler: async function ({ request, api, context }) {
        context.header = {}
        context.args = {}

        // #region [lấy tham số đầu vào]

        let header = { data: {}, schema: {} }
        let args = { data: {}, schema: {} }

        for (let key in api.parameter) {
            if (!api.parameter.hasOwnProperty(key)) continue
            let schema = api.parameter[key]
            let location = Location.get({ schema })

            switch (location) {
                case 'header':
                    header.schema[key] = schema
                    header.data[key] = request.header[key]
                    break
                case 'path':
                    args.schema[key] = schema
                    args.data[key] = api.path_regex.exec(request.url).groups[
                        key
                    ]
                    break
                case 'query':
                    args.schema[key] = schema
                    args.data[key] = request.query[key]

                    if (schema.type === 'array') {
                        if (args.data[key] === undefined) {
                            args.data[key] = request.query[key + '[]']
                        }

                        if (
                            !!args.data[key] &&
                            !Array.isArray(args.data[key])
                        ) {
                            args.data[key] = [args.data[key]]
                        }
                    }
                    break
                case 'body':
                    args.schema[key] = schema
                    args.data[key] = request.body[key]
                    break
            }
        }

        header.schema = Joi.object(header.schema)
        args.schema = Joi.object(args.schema)

        // #endregion

        // #region [validate]

        try {
            context.header = Joi.tryValidate({
                data: header.data,
                schema: header.schema,
            })

            context.args = Joi.tryValidate({
                data: args.data,
                schema: args.schema,
            })
        } catch (e) {
            if (
                !e.details ||
                !e.details[0] ||
                !e.details[0].type ||
                !e.details[0].context ||
                !e.details[0].context.label
            ) {
                throw new BaseError({
                    code: 'bad_request',
                    base_message: e.message,
                })
            }

            throw new HttpError({
                code: 'bad_request_' + e.details[0].type.replace(/\./g, '_'),
                http_code: 400,
                base_message: e.message,
                data: { field: e.details[0].context.label },
            })
        }

        // #endregion
    },
})
