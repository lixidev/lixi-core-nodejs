let { RequireModule } = require('../../../../package')
let Api = RequireModule('api')
let Joi = RequireModule('joi')

module.exports = Api.defineMiddleware({
    name: 'prepare',
    description: 'chuẩn bị tiến trình xử lý request',
    option: {
        meta_field: Joi.string()
            .default('meta')
            .description('trường dữ liệu meta'),
        data_field: Joi.string()
            .default('data')
            .description('trường dữ liệu data'),
        error_field: Joi.string()
            .default('error')
            .description('trường dữ liệu error'),
    },
    setup: async function ({ api }) {
        api.parameter.version = Joi.string()
            .meta({ location: 'header' })
            .description('phiên bản api sử dụng')
        api.parameter.client_platform = Joi.string()
            .valid('unknown', 'web', 'ios', 'android')
            .meta({ location: 'header' })
            .default('unknown')
            .description(
                'nền tảng, thiết bị của client đang truy cập'
            )
        api.parameter.client_version = Joi.string()
            .meta({ location: 'header' })
            .description('phiên bản của client đang truy cập')
    },
    handler: async function ({ response }) {
        response.type = 'application/json'
    },
})
