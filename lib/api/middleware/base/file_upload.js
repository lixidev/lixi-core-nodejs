let { RequireModule } = require('../../../../package')
let { BaseError } = RequireModule('err')
let Api = RequireModule('api')
let Joi = RequireModule('joi')
let Multer = require('multer')
let Path = require('path')
let Fs = require('fs')

module.exports = Api.defineMiddleware({
    name: 'file_upload',
    description: 'xử lý file upload',
    option: {
        field: Joi.string()
            .required()
            .description('tên field được sử dụng để upload file'),
        mode: Joi.string()
            .valid('single', 'array')
            .default('single')
            .description('dạng upload'),
        temp_dir: Joi.string()
            .default('upload')
            .description('thư mục tạm để lưu trữ file upload'),
        file_name_handler: Joi.func()
            .maxArity(1)
            .description('hàm phát sinh tên file'),
        allow_file_types: Joi.array()
            .items(Joi.string())
            .default([])
            .description('danh sách loại file cho phép'),
        max_file_size: Joi.number()
            .integer()
            .positive()
            .default(3000000)
            .description('kích thước file tối đa cho phép (bytes)'),
    },
    setup: async function ({ option }) {
        Fs.mkdirSync(option.temp_dir, { recursive: true })
    },
    handler: async function ({ request, response, context, option }) {
        return new Promise((resolve, reject) => {
            let config = {}

            // #region [cấu hình nơi lưu file]

            config.dest = option.temp_dir

            if (option.file_name_handler) {
                config.storage = Multer.diskStorage({
                    destination: (req, file, cb) => {
                        cb(null, option.temp_dir)
                    },
                    filename: (req, file, cb) => {
                        cb(null, option.file_name_handler({ context, file }))
                    },
                })
            }

            // #endregion

            // #region [file limit]

            config.limits = {
                fileSize: option.max_file_size,
            }

            // #endregion

            // #region [file filter]

            let allow_file_types = option.allow_file_types.filter(
                (x) => x !== '*.*'
            )

            config.fileFilter = (req, file, cb) => {
                let extname = Path.extname(file.originalname)
                if (
                    allow_file_types.length > 0 &&
                    !allow_file_types.includes(extname)
                ) {
                    cb(new BaseError({ code: 'file_type_not_allowed' }))
                    return
                }

                cb(null, true)
            }

            // #endregion

            let multer = Multer(config)[option.mode](option.field)
            multer(request.req, response.res, (err) => {
                // #region [phản hồi lỗi]

                if (err) {
                    let error_code = error_map[err.code]
                    if (error_code) reject(new BaseError({ code: error_code }))
                    else reject(err)
                    return
                }

                // #endregion

                // #region [xử lý kết quả phản hồi]

                if (request.req.files) context.files = request.req.files
                else context.files = [request.req.file]

                if (context.files) {
                    context.files = context.files.filter((x) => x !== undefined)
                }

                // #endregion

                resolve()
            })
        })
    },
})

let error_map = {
    LIMIT_FILE_SIZE: 'file_size_too_large',
    LIMIT_UNEXPECTED_FILE: 'file_field_invalid',
}
