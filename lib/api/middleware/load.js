let { Core } = require('../../../package')
let { Instance, EnableValue, CheckExists } = Core

module.exports = Instance.registerMethod({
    description: 'nạp middleware',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            api_server: joi
                .object()
                .required()
                .description('api server nạp middleware'),
            default_value: joi
                .object({
                    enable: joi
                        .boolean()
                        .required()
                        .description('bật/tắt middleware hay không?'),
                })
                .required()
                .description('các giá trị mặc định trong workflow'),
        }),
    handler: async function ({ api_server, default_value }) {
        // #region [enable & exists]

        let enable = await EnableValue({
            target: this,
            input: { api_server },
        })
        if (!enable) return

        CheckExists({
            item: this,
            list: api_server.middlewares,
            error_message: 'load api middleware failed',
        })

        // #endregion

        // #region [ghi nhận]

        this.default_value = default_value
        api_server.middlewares.push(this)

        // #endregion
    },
})
