/** kiểm tra response */

module.exports = function validate({ response, _path }) {
    if (!_path) _path = 'response'

    if (!allowed_types.includes(response.type)) {
        throw Error(
            `"${_path}" failed because [type must be one of ${allowed_types.join(
                ', '
            )}]`
        )
    }

    if (response.type === 'array') {
        if (response.$_terms.items.length !== 1) {
            throw Error(`"${_path}" failed because [must have 1 item type]`)
        }

        validate({
            response: response.$_terms.items[0],
            _path,
        })
    }

    if (response.type === 'object') {
        if (!response.$_terms.keys || response.$_terms.keys.length <= 0) {
            throw Error(
                `"${_path}" failed because [must have at least 1 field]`
            )
        }

        for (let { key, schema } of response.$_terms.keys) {
            validate({
                response: schema,
                _path: _path + '.' + key,
            })
        }
    }
}

let allowed_types = [
    'boolean',
    'number',
    'string',
    'timestamp',
    'phone',
    'array',
    'object',
    'any',
]
