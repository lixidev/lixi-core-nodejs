/** kiểm tra parameter */

module.exports = function validate({ parameter, location, _path }) {
    if (!_path) _path = 'parameter'
    let allowed_types = list_allowed_type[location]

    if (!allowed_types.includes(parameter.type)) {
        throw Error(
            `"${_path}" failed because [type must be one of ${allowed_types.join(
                ', '
            )}]`
        )
    }

    if (parameter.type === 'array') {
        if (parameter.$_terms.items.length !== 1) {
            throw Error(`"${_path}" failed because [must have 1 item type]`)
        }

        validate({
            parameter: parameter.$_terms.items[0],
            location,
            _path,
        })
    }

    if (parameter.type === 'object') {
        if (!parameter.$_terms.keys || parameter.$_terms.keys.length <= 0) {
            throw Error(
                `"${_path}" failed because [must have at least 1 field]`
            )
        }

        for (let { key, schema } of parameter.$_terms.keys) {
            validate({
                parameter: schema,
                location,
                _path: _path + '.' + key,
            })
        }
    }
}

let list_allowed_type = {
    path: ['boolean', 'number', 'string', 'timestamp', 'phone'],
    header: ['boolean', 'number', 'string', 'timestamp', 'phone', 'array'],
    query: ['boolean', 'number', 'string', 'timestamp', 'phone', 'array'],
    body: [
        'boolean',
        'number',
        'string',
        'timestamp',
        'phone',
        'array',
        'object',
        'any',
    ],
}
