/** kiểm tra tên của các field */

module.exports = function validate({ schema, _path }) {
    if (
        schema.type !== 'object' ||
        !schema.$_terms.keys ||
        schema.$_terms.keys.length === 0
    ) {
        return
    }

    if (!_path) _path = schema._flags.label

    for (let item of schema.$_terms.keys) {
        if (!/^[a-z0-9_.]+$/g.test(item.key)) {
            throw Error(
                `"${_path}.${item.key}" failed because ["${item.key}" is invalid "^[a-z0-9_.]+$" pattern]`
            )
        }

        if (item.schema.type === 'object') {
            validate({
                schema: item.schema,
                _path: _path + '.' + item.key,
            })
        }
    }
}
