module.exports = [
    {
        code: 'exception',
        message: 'exception server error',
        http_code: 500,
    },
    {
        code: 'endpoint_not_found',
        message: 'request end-point not found',
        http_code: 404,
    },
    {
        code: 'version_invalid',
        message: 'version is missing or invalid',
        http_code: 400,
    },
    {
        code: 'bad_request',
        message: 'bad request or invalid parameter',
        http_code: 400,
    },
    {
        code: 'file_field_invalid',
        message: `field upload file is missing or invalid`,
        http_code: 400,
    },
    {
        code: 'file_type_not_allowed',
        message: `upload file's type is not allowed`,
        http_code: 400,
    },
    {
        code: 'file_size_too_large',
        message: `upload file's size is too large`,
        http_code: 400,
    },
    {
        code: 'bad_response',
        message: 'bad response from server',
        http_code: 500,
    },
]
