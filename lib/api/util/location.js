/** các hàm hỗ trợ về location của parameter */

module.exports = {
    get: function ({ schema }) {
        let meta = schema.$_terms.metas.find((x) => x.location)

        if (!meta) return null
        return meta.location
    },

    default: function ({ method }) {
        if (method === 'post' || method === 'put') return 'body'
        return 'query'
    },
}
