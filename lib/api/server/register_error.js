let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'đăng ký mã lỗi',
    key: 'registerError',
    parameter: (joi) =>
        joi.object({
            code: joi.string().required().description('mã lỗi'),
            http_code: joi.number().description('mã lỗi http'),
            message: joi.string().description('mô tả lỗi'),
        }),
    handler: function ({ code, http_code, message }) {
        let error = this.errors.find((x) => x.code === code)

        if (error) {
            if (http_code) error.http_code = http_code
            if (message) error.message = message
        } else {
            this.errors.push({
                code,
                http_code,
                message,
            })
        }
    },
})
