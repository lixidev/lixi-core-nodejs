let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'lấy api middleware',
    key: 'getMiddleware',
    parameter: (joi) =>
        joi.object({
            name: joi.string().required().description('tên middleware cần lấy'),
        }),
    handler: function ({ name }) {
        return this.middlewares.find((x) => x.name === name)
    },
})
