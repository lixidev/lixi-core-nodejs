let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'lấy api end-point',
    key: 'getEndpoint',
    parameter: (joi) =>
        joi.object({
            method: joi
                .string()
                .required()
                .description('http method của end-point'),
            path: joi
                .string()
                .required()
                .description('đường dẫn của end-point'),
            version: joi
                .string()
                .required()
                .description('phiên bản của end-point'),
        }),
    handler: function ({ method, path, version }) {
        return this.apis.find(
            (x) =>
                x.method === method &&
                x.path_regex.test(path) &&
                x.matched_versions.includes(version)
        )
    },
})
