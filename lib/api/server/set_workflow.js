let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'đặt workflow',
    key: 'setWorkflow',
    parameter: (joi) =>
        joi.object({
            workflow: joi
                .array()
                .items(joi.string())
                .required()
                .description('workflow cần đặt'),
        }),
    handler: function ({ workflow }) {
        // #region [validate middleware]

        for (let middleware_name of workflow) {
            if (!this.middlewares.find((x) => x.name === middleware_name)) {
                throw Error(
                    `set global workflow failed, middleware "${middleware_name}" is un-registered or disabled`
                )
            }
        }

        // #endregion

        // #region [validate must-have middleware]

        let missing_items = []
        for (let item_name of must_have_workflow_items) {
            if (workflow.includes(item_name)) continue
            missing_items.push(item_name)
        }

        if (missing_items.length > 0) {
            missing_items = missing_items.join(', ')
            throw Error(`workflow must have these item: "${missing_items}"`)
        }

        // #endregion

        this.workflow = workflow
    },
})

let must_have_workflow_items = [
    'prepare',
    'parse_parameter',
    'run_handler',
    'parse_response',
]
