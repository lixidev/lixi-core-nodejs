let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let { BaseError } = RequireModule('err')
let Util = RequireModule('util')
let Semver = require('semver')

let Metadata = require('../metadata/_')

module.exports = Instance.registerMethod({
    description: 'xử lý request',
    key: 'requestProcess',
    handler: async function (ctx) {
        let { request, response } = ctx
        let service = this.service

        // #region [chuẩn bị context]

        let context = {
            skip_step: [],
            http_status_code: null,
            meta: {
                request_uuid: Util.randomUuid(),
                request_time: new Date(),
            },
            result: {},
            error: null,
        }

        // #endregion

        // #region [xác định path của request]

        let path
        if (request.url.indexOf('?') > -1) {
            path = request.url.substr(0, request.url.indexOf('?'))
        } else path = request.url

        // #endregion

        // #region [metadata end-point]

        if (path.startsWith(Metadata.path)) {
            try {
                await Metadata.requestProcess({
                    path,
                    service,
                    request,
                    response,
                })
            } catch (e) {
                await endHandle.bind(this)({
                    ctx,
                    service,
                    api_server: this,
                    request,
                    response,
                    api: null,
                    context,
                    exception: e,
                })
            }

            return
        }

        // #endregion

        // #region [xác định version]

        let version = this.option.default_version
        // if (request.header.version) {
        //     version = Semver.valid(request.header.version)
        // } else {
        //     version = this.option.default_version
        // }

        // if (!version || !this.list_version.includes(version)) {
        //     version = this.option.default_version
        //     // await endHandle.bind(this)({
        //     //     ctx,
        //     //     service,
        //     //     api_server: this,
        //     //     request,
        //     //     response,
        //     //     api: null,
        //     //     context,
        //     //     exception: new BaseError({ code: 'version_invalid' }),
        //     // });
        //     // return;
        // }

        // #endregion

        // #region [tìm end-point]

        let api = this.getEndpoint({
            method: request.method.toLowerCase(),
            path,
            version,
        })

        if (!api) {
            await endHandle.bind(this)({
                ctx,
                service,
                api_server: this,
                request,
                response,
                api,
                context,
                exception: new BaseError({ code: 'endpoint_not_found' }),
            })
            return
        }

        // #endregion

        // #region [xử lý theo workflow]

        for (let { name, handler, option } of api.workflow) {
            if (context.skip_step.includes(name)) continue

            try {
                await handler({
                    ctx,
                    service,
                    api_server: this,
                    request,
                    response,
                    api,
                    context,
                    option,
                })
            } catch (e) {
                await endHandle.bind(this)({
                    ctx,
                    service,
                    api_server: this,
                    request,
                    response,
                    api,
                    context,
                    exception: e,
                })
                return
            }
        }

        // #endregion

        // #region [send response]

        if (!context.skip_step.includes('send_response')) {
            await endHandle.bind(this)({
                ctx,
                service,
                api_server: this,
                request,
                response,
                api,
                context,
            })
        }

        // #endregion
    },
})

async function endHandle({
    ctx,
    service,
    api_server,
    request,
    response,
    api,
    context,
    exception,
}) {
    context.meta.response_time = new Date()

    if (exception) {
        await this.getMiddleware({ name: 'error_handler' }).handler({
            ctx,
            service,
            api_server,
            request,
            response,
            api,
            context,
            exception,
        })
    }

    await this.getMiddleware({ name: 'send_response' }).handler({
        ctx,
        service,
        api_server,
        request,
        response,
        api,
        context,
    })
}
