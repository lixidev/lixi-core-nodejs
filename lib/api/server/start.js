let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'khởi chạy server',
    key: 'start',
    handler: function () {
        if (this._instance) return

        if (!this.is_initialized) {
            this.koa.use(this.requestProcess)
            this.is_initialized = true
        }

        this._instance = this.koa.listen(this.option.port)
    },
})
