let { Core } = require('../../../package')
let { Instance, VersionValidation } = Core
let Koa = require('koa')
let Body = require('koa-body')
let Cors = require('@koa/cors')

let BaseMiddleware = require('../middleware/base/_')
let BaseError = require('../util/base_errors')

module.exports = Instance.registerMethod({
    description: 'tạo server',
    key: 'createServer',
    parameter: (joi) =>
        joi.object({
            koa: joi.object().description('koa instance sử dụng'),
            protocol: joi
                .string()
                .default('http')
                .description('protocol sử dụng cho api'),
            host: joi
                .string()
                .default('localhost')
                .description('host sử dụng cho api'),
            port: joi
                .number()
                .default(8080)
                .description('port sử dụng cho api'),
            public_domain: joi
                .string()
                .description('public domain để truy cập vào api'),
            show_error_stack: joi
                .boolean()
                .default(true)
                .description('hiển thị slack của lỗi'),
            list_version: joi
                .array()
                .items(joi.string().custom(VersionValidation))
                .min(1)
                .required()
                .description('danh sách version cho phép chạy'),
            default_version: joi
                .string()
                .custom(VersionValidation)
                .required()
                .description('version mặc định cho mỗi request'),
        }),
    handler: async function ({
        koa,
        protocol,
        host,
        port,
        public_domain,
        show_error_stack,
        list_version,
        default_version,
    }) {
        // #region [validate]

        if (!list_version.includes(default_version)) {
            let allow_versions = list_version.join(', ')
            throw Error(
                `default version is invalid, must be one of ${allow_versions}`
            )
        }

        // #endregion

        // #region [tạo instance và option]

        let option = {
            protocol,
            host,
            port,
            public_domain,
            show_error_stack,
            default_version,
        }

        let server = Instance.create({
            initial: {
                option,
                list_version,
                middlewares: [],
                apis: [],
                errors: [],
                workflow: [],
            },
            extensions: [
                require('./get_endpoint'),
                require('./get_middleware'),
                require('./register_error'),
                require('./register_list_error'),
                require('./request_process'),
                require('./set_workflow'),
                require('./start'),
                require('./stop'),
            ],
        })

        // #endregion

        // #region [khởi tạo koa]

        if (koa) server.koa = koa
        else {
            server.koa = new Koa()
            server.koa.use(Body())
            server.koa.use(Cors())
        }

        // #endregion

        // nạp các base middleware
        await BaseMiddleware({ api_server: server })

        // đăng ký các mã lỗi
        server.registerListError({ list_error: BaseError })

        return server
    },
})
