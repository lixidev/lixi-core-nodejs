let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'đăng ký danh sách mã lỗi',
    key: 'registerListError',
    parameter: (joi) =>
        joi.object({
            list_error: joi
                .array()
                .items(
                    joi.object({
                        code: joi.string().required().description('mã lỗi'),
                        http_code: joi.number().description('mã lỗi http'),
                        message: joi.string().description('mô tả lỗi'),
                    })
                )
                .min(1)
                .required()
                .description('danh sách mã lỗi sẽ đăng ký'),
        }),
    handler: function ({ list_error }) {
        for (let error of list_error) {
            this.registerError(error)
        }
    },
})
