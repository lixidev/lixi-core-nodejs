let { Core } = require('../../../../package')
let { Instance } = Core

module.exports = Instance.create({
    extensions: [require('./healthcheck'), require('./info')],
})
