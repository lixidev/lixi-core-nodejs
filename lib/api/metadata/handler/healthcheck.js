let { Core } = require('../../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'kiêm tra server còn sống hay chết',
    key: 'healthcheck',
    parameter: require('./_parameter'),
    handler: async function ({ response }) {
        response.status = 200
        response.body = `I'm still alive`
    },
})
