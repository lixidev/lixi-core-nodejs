module.exports = (joi) =>
    joi.object({
        service: joi.object().required().description('service vận hành'),
        request: joi.any().required().description('request của server engine'),
        response: joi
            .any()
            .required()
            .description('response của server engine'),
    })
