let { Core } = require('../../../../package')
let { Instance } = Core
let Path = require('path')

module.exports = Instance.registerMethod({
    description: 'lấy thông tin server',
    key: 'info',
    parameter: require('./_parameter'),
    handler: async function ({ service, response }) {
        let package_info = require(Path.join(
            service.option.root_dir_path,
            'package.json'
        ))

        response.status = 200
        response.type = 'application/json'
        response.body = { version: package_info.version || 'undefined' }
    },
})
