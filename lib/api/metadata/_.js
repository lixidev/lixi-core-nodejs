let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.create({
    initial: {
        path: require('./path'),
    },
    extensions: [require('./request_process')],
})
