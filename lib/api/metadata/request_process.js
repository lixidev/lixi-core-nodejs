let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let { BaseError } = RequireModule('err')

let MetadataPath = require('./path')
let Handler = require('./handler/_')

module.exports = Instance.registerMethod({
    description: 'hàm xử lý gói tin',
    key: 'requestProcess',
    parameter: (joi) =>
        joi.object({
            path: joi.string().required().description('path'),
            service: joi.object().required().description('service vận hành'),
            request: joi
                .any()
                .required()
                .description('request của server engine'),
            response: joi
                .any()
                .required()
                .description('response của server engine'),
        }),
    handler: async function ({ path, service, request, response }) {
        let key = path.substr(MetadataPath.length + 1)
        if (!Handler.hasOwnProperty(key)) {
            throw new BaseError({ code: 'endpoint_not_found' })
        }

        await Handler[key]({ service, request, response })
    },
})
