let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerProperty({
    description: 'tên của end-point',
    key: 'name',
    get: function () {
        return this.method.toUpperCase() + ' ' + this.path.substr(1)
    },
})
