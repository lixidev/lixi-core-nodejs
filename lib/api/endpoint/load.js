let { Core, RequireModule } = require('../../../package')
let { Instance, EnableValue, CheckExists, Location, SetDefaultResponse } = Core
let Joi = RequireModule('joi')
let Semver = require('semver')

let ValidateFieldKey = require('../util/validate_field_key')
let ValidateParameter = require('../util/validate_parameter')
let ValidateResponse = require('../util/validate_response')

module.exports = Instance.registerMethod({
    description: 'nạp api end-point',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp api'),
            _module: joi.object().required().description('module nạp api'),
        }),
    handler: async function ({ service, _module }) {
        // #region [enable]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })
        if (!enable) return

        // #endregion

        // #region [xác định các version]

        this.matched_versions = []
        for (let version of service.api_server.list_version) {
            if (typeof this.version_range === 'string') {
                if (!Semver.satisfies(version, this.version_range)) continue
            } else {
                if (!this.version_range.find((x) => x === version)) continue
            }

            this.matched_versions.push(version)
        }

        if (this.matched_versions.length === 0) return

        // #endregion

        // #region [xác định key]

        let key = this.method + '_' + this.path.substr(1).replace(/\//g, '_')
        for (let version of this.matched_versions) {
            key += '_' + version
        }
        this.key = key

        // #endregion

        // #region [check exists]

        CheckExists({
            item: this,
            list: service.api_server.apis,
            error_message: 'load api endpoint failed',
        })

        // #endregion

        // #region [validate middleware]

        if (this.middlewares) {
            for (let middleware of this.middlewares) {
                let endpoint_middleware = this.middlewares.find(
                    (x) => x.name === middleware.name
                )

                if (!endpoint_middleware) {
                    throw Error(
                        `load api end-point "${this.name}" failed, middleware "${middleware.name}" is un-registered or disabled`
                    )
                }
            }
        }

        // #endregion

        // #region [get apply middleware]

        let apply_middlewares = []
        for (let middleware of service.api_server.middlewares) {
            let { enable } = middleware.default_value
            let option

            if (this.middlewares) {
                endpoint_middleware = this.middlewares.find(
                    (x) => x.name === middleware.name
                )

                if (endpoint_middleware) {
                    enable = endpoint_middleware.enable
                    if (endpoint_middleware.option) {
                        option = endpoint_middleware.option
                    }
                }
            }

            if (!enable) continue

            if (middleware.option) {
                option = Joi.tryValidate({
                    data: option,
                    schema: middleware.option,
                })
            } else option = undefined

            apply_middlewares.push({
                key: middleware.key,
                name: middleware.name,
                enable,
                option,
                setup: middleware.setup,
                handler: middleware.handler,
            })
        }

        // #endregion

        // #region [setup workflow]

        this.workflow = []
        for (let middleware_name of service.api_server.workflow) {
            let middleware = apply_middlewares.find(
                (x) => x.name === middleware_name
            )
            if (!middleware || !middleware.enable) continue

            if (middleware.handler) {
                this.workflow.push({
                    key: middleware.key,
                    name: middleware.name,
                    option: middleware.option,
                    handler: middleware.handler,
                })
            }
        }

        // #endregion

        // #region [run setup from middleware]

        for (let middleware of apply_middlewares) {
            if (!middleware.enable || !middleware.setup) continue

            await middleware.setup({
                service,
                _module,
                api_server: service.api_server,
                api: this,
                option: middleware.option,
            })
        }

        // #endregion

        // #region [chuẩn hóa các parameter được thêm bởi middleware]

        // #region [validate parameter]

        if (this.parameter && Object.keys(this.parameter).length > 0) {
            for (let key in this.parameter) {
                if (!this.parameter.hasOwnProperty(key)) continue

                this.parameter[key] = this.parameter[key].label(
                    'parameter.' + key
                )

                let location = Location.get({ schema: this.parameter[key] })
                if (!location) {
                    location = Location.default({ method: this.method })
                    this.parameter[key] = this.parameter[key].meta({
                        location,
                    })
                } else {
                    Joi.tryValidate({
                        data: location,
                        schema: Joi.string()
                            .valid('path', 'query', 'header', 'body')
                            .label(`parameter.${key}:location`),
                    })
                }

                ValidateParameter({
                    parameter: this.parameter[key],
                    location,
                    _path: 'parameter.' + key,
                })
            }

            if (!this.option.unsafe_parameter_response_key) {
                ValidateFieldKey({
                    schema: Joi.object(this.parameter).label('parameter'),
                })
            }
        }

        // #endregion

        // #region [validate response]

        if (this.response) {
            this.response = this.response.required().label('response')
            this.response = SetDefaultResponse({ response: this.response })

            if (!this.option.unsafe_parameter_response_key) {
                ValidateFieldKey({ schema: this.response })
            }

            ValidateResponse({ response: this.response })
        }

        // #endregion

        // #region [xác định load order]

        this.load_order = 0
        if (this.path.includes(':')) this.load_order += 1

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        service.api_server.apis.push(this)
        _module.apis.push(this)

        service.api_server.apis.sort((a, b) => a.load_order - b.load_order)
        _module.apis.sort((a, b) => a.load_order - b.load_order)

        // #endregion
    },
})
