let { Core } = require('../../../package')
let { Instance, FileInfo, VersionValidation } = Core
let Metadata = require('../metadata/_')

module.exports = Instance.registerMethod({
    description: 'khai báo API end-point',
    key: 'defineEndpoint',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            unsafe_parameter_response_key: joi
                .boolean()
                .description(
                    'không tiến hành kiểm tra tên parameter và response field?s'
                )
                .default(false),
            method: joi
                .string()
                .required()
                .valid('get', 'post', 'put', 'delete')
                .description('http method của end-point'),
            path: joi
                .string()
                .required()
                .regex(/^\//, '"must starts with /"')
                .description('đường dẫn đến end-point'),
            version_range: joi
                .alternatives()
                .try(
                    joi.string().disallow(''),
                    joi
                        .array()
                        .items(joi.string().custom(VersionValidation))
                        .min(1)
                )
                .default('*')
                .description('các phiên bản mà end-point áp dụng'),
            description: joi.string().description('mô tả về end-point'),
            parameter: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .default({})
                .description('tham số đầu vào của end-point'),
            response: joi
                .object()
                .schema()
                .description('kết quả trả về của end-point'),
            middlewares: joi
                .array()
                .items(
                    joi.object({
                        name: joi
                            .string()
                            .required()
                            .description('tên middleware'),
                        enable: joi
                            .boolean()
                            .required()
                            .description('bật/tắt middleware'),
                        option: joi
                            .object()
                            .pattern(joi.string(), joi.any())
                            .description('tham số dành cho middleware'),
                    })
                )
                .description('danh sách các middleware của end-point'),
            handler: joi
                .func()
                .maxArity(1)
                .description('hàm xử lý gói tin của end-point'),
            error_handler: joi
                .func()
                .maxArity(1)
                .description('hàm xử lý lỗi của end-point'),
            success_meta_handler: joi
                .func()
                .maxArity(1)
                .description('hàm xử lý success meta của end-point'),
            error_meta_handler: joi
                .func()
                .maxArity(1)
                .description('hàm xử lý error meta của end-point'),
        }),
    handler: function ({
        enable,
        unsafe_parameter_response_key,
        method,
        path,
        version_range,
        description,
        parameter,
        response,
        middlewares,
        handler,
        error_handler,
        success_meta_handler,
        error_meta_handler,
    }) {
        // #region [validate path]

        if (path.startsWith(Metadata.path)) {
            throw Error('cannot overwrite metadata APIs')
        }

        // #endregion

        // #region [tạo instance & option]

        let api = {
            enable,
            method,
            path,
            version_range,
            description,
            parameter,
            response,
            middlewares,
            handler,
            error_handler,
            success_meta_handler,
            error_meta_handler,
            option: { unsafe_parameter_response_key },
        }

        let file_info = FileInfo()
        api.file_path = file_info.path

        // #endregion

        // #region [tạo path regex để phục vụ tìm kiếm api]

        let path_regex = api.path

        if (path_regex.includes(':')) {
            path_regex = path_regex.split('/')
            for (let i = 0; i < path_regex.length; i++) {
                if (path_regex[i].startsWith(':')) {
                    path_regex[i] =
                        '(?<' + path_regex[i].substr(1) + '>[\\w\\.]+)'
                }
            }
            path_regex = path_regex.join('/')
        }

        path_regex += '(\\?.+)?'

        api.path_regex = new RegExp('^' + path_regex + '$')

        // #endregion

        return Instance.create({
            initial: api,
            extensions: [require('./load'), require('./name')],
        })
    },
})
