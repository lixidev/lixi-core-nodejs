let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện thao tác elasticsearch',
    extensions: [require('./instance/_init')],
})
