let { RequireModule } = require('../../../package')
let ElasticSearch = RequireModule('elasticsearch')
let Joi = RequireModule('joi')

test('init', () => {
    let es = ElasticSearch.init(require('./_config'))

    Joi.tryValidate({ data: es, schema: Joi.object() })
})
