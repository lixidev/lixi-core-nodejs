let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Joi = RequireModule('joi')

module.exports = Instance.registerMethod({
    description: 'chuyển đổi đối tượng sang chuẩn elasticsearch',
    key: 'parse',
    parameter: (joi) =>
        joi.object({
            data: joi.object().required().description('dữ liệu cần chuyển đổi'),
            model: joi.object().description('schema mô tả dữ liệu'),
        }),
    handler: function ({ data, model_schema }) {
        return convert({ data, model_schema })
    },
})

function convert({ data, model_schema }) {
    if (Array.isArray(data)) {
        let result = []

        for (let i = 0; i < data.length; i++) {
            result.push(
                convert({
                    data: result[i],
                    model_schema,
                })
            )
        }

        return result
    }

    let result
    if (model_schema) {
        result = {}

        for (let key in model_schema.fields) {
            if (!data.hasOwnProperty(key)) continue
            let field = model_schema.fields[key]

            if (field.type === 'boolean' && [0, 1].includes(data[key])) {
                result[key] = data[key] === 1
            } else result[key] = data[key]
        }

        result = Joi.tryValidate({
            data: result,
            schema: model_schema.joi_schema,
        })
    } else result = Object.assign({}, data)

    return result
}
