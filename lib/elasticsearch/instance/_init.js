let { Core } = require('../../../package')
let { Instance } = Core
let Elasticsearch = require('@elastic/elasticsearch')

module.exports = Instance.registerMethod({
    description: 'khởi tạo elasticsearch client',
    key: 'init',
    parameter: (joi) =>
        joi.object({
            node: joi
                .string()
                .uri()
                .required()
                .description('url đến server elastic'),
            prefix: joi
                .string()
                .description('tiền tố cho index/bảng')
                .default(''),
            postfix: joi
                .string()
                .description('hậu tố cho index/bảng')
                .default(''),
            config: joi
                .object()
                .description('các cấu hình cho elasticsearch client'),
            auto_type: joi
                .boolean()
                .default(true)
                .description('tự động gán type = index khi query?'),
            keep_alive: joi
                .boolean()
                .default(false)
                .description('có giữ kết nối đến server hay không?'),
        }),
    handler: function ({
        node,
        prefix,
        postfix,
        config,
        auto_type,
        keep_alive,
    }) {
        let es = {
            option: { prefix, postfix, auto_type, keep_alive },
        }

        // #region [tạo client]

        if (!config) config = {}
        config = Object.assign({ node, keep_alive }, config)
        es.client = new Elasticsearch.Client(config)

        // #endregion

        // #region [tạo instance]

        return Instance.create({
            initial: es,
            extensions: [
                require('./parse'),
                require('../indices/_indices'),
                require('../query/_init'),
            ],
        })

        // #endregion
    },
})
