let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tạo bảng',
    key: 'createTable',
    parameter: (joi) =>
        joi.object({
            table_name: joi.string().required().description('tên bảng cần tạo'),
            mappings: joi.object().required().description('danh sách field'),
            settings: joi.object().description('các thiết lập thêm'),
        }),
    handler: async function ({ table_name, mappings, settings }) {
        return await this._es.client.indices.create({
            index:
                this._es.option.prefix + table_name + this._es.option.postfix,
            body: { mappings, settings },
        })
    },
})
