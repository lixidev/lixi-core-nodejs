let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerExtension({
    description: 'elasticsearch indices',
    key: 'indices',
    extension: function () {
        return Instance.create({
            initial: { _es: this },
            extensions: [
                require('./create_table'),
                require('./drop_table'),
                require('./put_mapping'),
            ],
        })
    },
})
