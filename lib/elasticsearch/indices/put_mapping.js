let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'cập nhật mapping của bảng',
    key: 'putMapping',
    parameter: (joi) =>
        joi.object({
            table_name: joi
                .string()
                .required()
                .description('tên bảng cần chỉnh sửa'),
            mappings: joi.object().required().description('danh sách field'),
        }),
    handler: async function ({ table_name, mappings }) {
        return await this._es.client.indices.putMapping({
            index:
                this._es.option.prefix + table_name + this._es.option.postfix,
            type: table_name,
            body: { properties: mappings },
        })
    },
})
