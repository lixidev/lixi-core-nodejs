let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'xóa bảng',
    key: 'dropTable',
    parameter: (joi) =>
        joi.object({
            table_name: joi.string().required().description('tên bảng cần xóa'),
        }),
    handler: async function ({ table_name }) {
        return await this._es.client.indices.delete({
            index:
                this._es.option.prefix + table_name + this._es.option.postfix,
        })
    },
})
