let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set filter should',
    key: 'filterShould',
    parameter: (joi) =>
        joi
            .alternatives()
            .try(joi.object(), joi.array().items(joi.object()))
            .description('filter should cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.filter_should
            return this
        }

        if (!this._body.filter_should) this._body.filter_should = []

        if (Array.isArray(data)) {
            this._body.filter_should = this._body.filter_should.concat(data)
        } else this._body.filter_should.push(data)

        return this
    },
})
