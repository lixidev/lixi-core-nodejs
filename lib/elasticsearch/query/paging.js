let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set paging',
    key: 'paging',
    parameter: (joi) =>
        joi.object({
            page: joi
                .number()
                .min(1)
                .required()
                .description('vị trí trang dữ liệu'),
            page_size: joi
                .number()
                .min(1)
                .required()
                .description('kích thước trang dữ liệu'),
        }),
    handler: function ({ page, page_size }) {
        if (!data) {
            delete this._body.from
            delete this._body.size
            return this
        }

        this._body.from = (page - 1) * page_size
        this._body.size = page_size

        return this
    },
})
