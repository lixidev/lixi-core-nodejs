let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện truy vấn exists',
    key: 'exists',
    handler: async function () {
        return (
            await this._es.client.exists({
                index: this._body.index,
                type: this._body.type,
                id: this._body.id,
            })
        ).body
    },
})
