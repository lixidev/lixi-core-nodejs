let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set id',
    key: 'id',
    parameter: (joi) => joi.string().description('id cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.id
            return this
        }

        this._body.id = data

        return this
    },
})
