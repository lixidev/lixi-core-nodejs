let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện truy vấn aggregate',
    key: 'aggregate',
    handler: async function () {
        let data = await this._es.client.search({
            index: this._body.index,
            body: {
                query: {
                    bool: {
                        must: this._body.filter_must || [],
                        should: this._body.filter_should || [],
                        must_not: this._body.filter_must_not || [],
                    },
                },
                aggregations: this._body.aggregations,
            },
        })

        return data.body.aggregations
    },
})
