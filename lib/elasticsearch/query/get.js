let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện truy vấn get',
    key: 'get',
    handler: async function () {
        let result

        if (!!this._body.id) {
            try {
                result = await this._es.client.get({
                    index: this._body.index,
                    type: this._body.type,
                    id: this._body.id,
                })
                result = result.body
            } catch (e) {
                if (e.meta && e.meta.statusCode === 404) result = null
                else throw e
            }
        } else {
            result = await this._es.client.search({
                index: this._body.index,
                body: {
                    query: {
                        bool: {
                            must: this._body.filter_must || [],
                            should: this._body.filter_should || [],
                            must_not: this._body.filter_must_not || [],
                        },
                    },
                    sort: this._body.sort || [],
                    from: this._body.from || 0,
                    size: 1,
                },
            })
            result = result.body.hits.hits[0]
        }

        if (!result) return null
        return result._source
    },
})
