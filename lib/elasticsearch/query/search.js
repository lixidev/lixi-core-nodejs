let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện truy vấn search',
    key: 'search',
    handler: async function () {
        let data = await this._es.client.search({
            index: this._body.index,
            body: {
                query: {
                    bool: {
                        must: this._body.filter_must || [],
                        should: this._body.filter_should || [],
                        must_not: this._body.filter_must_not || [],
                    },
                },
                highlight: this._body.highlight,
                search_after: this._body.search_after || undefined,
                sort: this._body.sort || [],
                from: this._body.from || 0,
                size: this._body.size || 10,
            },
        })

        let total
        if (!data.body.hits.total) total = 0
        else {
            if (typeof data.body.hits.total === 'number') {
                total = data.body.hits.total
            } else if (data.body.hits.total.value) {
                total = data.body.hits.total.value
            } else total = 0
        }

        let items = []
        for (let hit of data.body.hits.hits) {
            let item = {
                ...hit._source,
                _id: hit._id,
                _score: hit._score,
                _highlight: hit.highlight,
            }
            items.push(item)
        }

        return { total, items }
    },
})
