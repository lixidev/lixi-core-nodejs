let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set index',
    key: 'index',
    parameter: (joi) => joi.string().description('index cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.index
            delete this._body.type
            return this
        }

        this._body.index =
            this._es.option.prefix + data + this._es.option.postfix
        if (this._es.option.auto_type && !this._body.type)
            this._body.type = this._body.index

        return this
    },
})
