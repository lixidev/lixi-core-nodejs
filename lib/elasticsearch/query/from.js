let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set from',
    key: 'from',
    parameter: (joi) => joi.number().description('from cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.from
            return this
        }

        this._body.from = data

        return this
    },
})
