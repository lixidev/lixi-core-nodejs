let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện lệnh bulk',
    key: 'bulk',
    parameter: (joi) =>
        joi.object({
            body: joi
                .array()
                .items(joi.object())
                .required()
                .description('danh sách các lệnh thực hiện'),
            refresh: joi
                .boolean()
                .default(false)
                .description('các chế độ refresh document'),
        }),
    handler: async function ({ body, refresh }) {
        let data = await this._es.client.bulk({
            body,
            refresh,
        })
        data = data.body

        if (data.errors) throw data
        return data
    },
})
