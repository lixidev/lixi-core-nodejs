let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set aggregations',
    key: 'aggregations',
    parameter: (joi) => joi.object().description('aggregations cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.aggregations
            return this
        }

        this._body.aggregations = data

        return this
    },
})
