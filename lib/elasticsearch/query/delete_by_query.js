let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện lệnh delete by query',
    key: 'deleteByQuery',
    parameter: (joi) =>
        joi.object({
            refresh: joi
                .alternatives()
                .try(joi.boolean(), joi.string().valid('wait_for'))
                .default(false)
                .description('các chế độ refresh document'),
        }),
    handler: async function ({ refresh }) {
        return (
            await this._es.client.deleteByQuery({
                index: this._body.index,
                type: this._body.type,
                body: {
                    query: {
                        bool: {
                            must: this._body.filter_must || [],
                            should: this._body.filter_should || [],
                            must_not: this._body.filter_must_not || [],
                        },
                    },
                },
                refresh,
            })
        ).body
    },
})
