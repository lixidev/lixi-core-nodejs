let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện lệnh update',
    key: 'update',
    parameter: (joi) =>
        joi.object({
            data: joi.object().required().description('dữ liệu cần thêm'),
            refresh: joi
                .alternatives()
                .try(joi.boolean(), joi.string().valid('wait_for'))
                .default(false)
                .description('các chế độ refresh document'),
        }),
    handler: async function ({ data, refresh }) {
        return (
            await this._es.client.update({
                index: this._body.index,
                type: this._body.type,
                id: this._body.id,
                body: { doc: data },
                refresh,
            })
        ).body
    },
})
