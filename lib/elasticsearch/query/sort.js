let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set sort',
    key: 'sort',
    parameter: (joi) =>
        joi
            .alternatives()
            .try(joi.object(), joi.array().items(joi.object()))
            .description('sort cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.sort
            return this
        }

        if (!this._body.sort) this._body.sort = []

        if (Array.isArray(data)) {
            this._body.sort = this._body.sort.concat(data)
        } else this._body.sort.push(data)

        return this
    },
})
