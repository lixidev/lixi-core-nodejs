let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set document type',
    key: 'type',
    parameter: (joi) => joi.string().description('document type cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.type
            return this
        }

        this._body.type = data

        return this
    },
})
