let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set search after',
    key: 'searchAfter',
    parameter: (joi) =>
        joi
            .array()
            .items(joi.any())
            .allow(null)
            .description('search after cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.search_after
            return this
        }

        this._body.search_after = data

        return this
    },
})
