let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện truy vấn count',
    key: 'count',
    handler: async function () {
        return (
            await this._es.client.count({
                index: this._body.index,
                type: this._body.type,
                body: {
                    query: {
                        bool: {
                            must: this._body.filter_must || [],
                            should: this._body.filter_should || [],
                            must_not: this._body.filter_must_not || [],
                        },
                    },
                },
            })
        ).body.count
    },
})
