let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set filter must',
    key: 'filterMust',
    parameter: (joi) =>
        joi
            .alternatives()
            .try(joi.object(), joi.array().items(joi.object()))
            .description('filter must cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.filter_must
            return this
        }

        if (!this._body.filter_must) this._body.filter_must = []

        if (Array.isArray(data)) {
            this._body.filter_must = this._body.filter_must.concat(data)
        } else this._body.filter_must.push(data)

        return this
    },
})
