let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set size',
    key: 'size',
    parameter: (joi) => joi.number().description('size cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.size
            return this
        }

        this._body.size = data

        return this
    },
})
