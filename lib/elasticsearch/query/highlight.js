let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set highlight',
    key: 'highlight',
    parameter: (joi) => joi.object().description('highlight cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.highlight
            return this
        }

        this._body.highlight = data
        
        return this
    },
})
