let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'set filter must not',
    key: 'filterMustNot',
    parameter: (joi) =>
        joi
            .alternatives()
            .try(joi.object(), joi.array().items(joi.object()))
            .description('filter must not cần set'),
    handler: function (data) {
        if (!data) {
            delete this._body.filter_must_not
            return this
        }

        if (!this._body.filter_must_not) this._body.filter_must_not = []

        if (Array.isArray(data)) {
            this._body.filter_must_not = this._body.filter_must_not.concat(data)
        } else this._body.filter_must_not.push(data)

        return this
    },
})
