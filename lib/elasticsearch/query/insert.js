let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện lệnh insert',
    key: 'insert',
    parameter: (joi) =>
        joi.object({
            data: joi
                .object({
                    id: joi
                        .number()
                        .required()
                        .description('id định danh dữ liệu'),
                })
                .unknown(true)
                .required()
                .description('dữ liệu cần thêm'),
            refresh: joi
                .alternatives()
                .try(joi.boolean(), joi.string().valid('wait_for'))
                .default(false)
                .description('các chế độ refresh document'),
        }),
    handler: async function ({ data, refresh }) {
        return (
            await this._es.client.index({
                index: this._body.index,
                type: this._body.type,
                id: data.id,
                body: data,
                refresh,
            })
        ).body
    },
})
