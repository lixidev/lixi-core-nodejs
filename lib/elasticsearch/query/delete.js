let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện lệnh delete',
    key: 'delete',
    parameter: (joi) =>
        joi.object({
            refresh: joi
                .alternatives()
                .try(joi.boolean(), joi.string().valid('wait_for'))
                .default(false)
                .description('các chế độ refresh document'),
        }),
    handler: async function ({ refresh }) {
        return (
            await this._es.client.delete({
                index: this._body.index,
                type: this._body.type,
                id: this._body.id,
                refresh,
            })
        ).body
    },
})
