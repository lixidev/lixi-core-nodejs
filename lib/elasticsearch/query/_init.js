let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tạo câu truy vấn',
    key: 'query',
    handler: function () {
        let query = { _es: this, _body: {} }

        return Instance.create({
            initial: query,
            extensions: [
                require('./index'),
                require('./type'),
                require('./id'),
                require('./filter_must'),
                require('./filter_should'),
                require('./filter_must_not'),
                require('./highlight'),
                require('./aggregations'),
                require('./search_after'),
                require('./sort'),
                require('./from'),
                require('./size'),
                require('./paging'),

                require('./get'),
                require('./exists'),
                require('./count'),
                require('./search'),
                require('./aggregate'),

                require('./bulk'),
                require('./insert'),
                require('./update'),
                require('./delete'),
                require('./delete_by_query'),
            ],
        })
    },
})
