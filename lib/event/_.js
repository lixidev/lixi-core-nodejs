let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện quản lý gói tin, sự kiện',
    extensions: [
        require('./instance/_init'),
        require('./publisher/_define'),
        require('./subscriber/_define'),
    ],
})
