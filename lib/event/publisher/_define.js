let { Core, RequireModule } = require('../../../package')
let { Instance, MakeKey } = Core
let Joi = RequireModule('joi')
let Callsite = require('callsite')

module.exports = Instance.registerMethod({
    description: 'đăng ký publisher',
    key: 'definePub',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            event: joi
                .string()
                .required()
                .description('event sẽ phát tín hiệu'),
            parameter: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .default({})
                .description('tham số chuyển phát'),
        }),
    handler: function ({ enable, event, parameter }) {
        // #region [tạo instance & option]

        let publisher = { enable, event }
        publisher.key = MakeKey({ text: event })
        publisher.file_path = Callsite()[2].getFileName()

        // #endregion

        // #region [parameter]

        if (parameter && Object.keys(parameter).length > 0) {
            parameter = Joi.object(parameter)
            parameter = parameter.default(parameter.validate().value)
            publisher.parameter = parameter
        }

        // #endregion

        return Instance.create({
            initial: publisher,
            extensions: [require('./load')],
        })
    },
})
