let { Core } = require('../../../package')
let { Instance, EnableValue, CheckExists } = Core

module.exports = Instance.registerMethod({
    description: 'nạp publisher',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp model'),
            _module: joi.object().required().description('module nạp model'),
        }),
    handler: async function ({ service, _module }) {
        // #region [enable & exists]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })
        if (!enable) return

        CheckExists({
            item: this,
            list: _module.event.publishers,
            error_message: 'load event publisher failed',
        })

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        _module.event.publishers.push(this)

        // #endregion
    },
})
