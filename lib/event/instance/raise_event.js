let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Joi = RequireModule('joi')
let Callsite = require('callsite')

module.exports = Instance.registerMethod({
    description: 'phát sinh sự kiện',
    key: 'raiseEvent',
    parameter: (joi) =>
        joi.object({
            event: joi.string().required().description('event sẽ phát sinh'),
            args: joi.any().description('dữ liệu của event được gửi đi'),
        }),
    handler: async function ({ event, args }) {
        let result = {
            success_count: 0,
            fail_count: 0,
            subscribers: [],
        }

        // #region [chuẩn bị gói tin]

        let publisher = this.publishers.find((x) => x.event === event)
        if (!publisher) throw Error(`publisher of "${event}" is not found`)

        if (publisher.parameter) {
            args = Joi.tryValidate({
                data: args,
                schema: publisher.parameter,
            })
        }

        publisher = { file_path: Callsite()[2].getFileName() }

        // #endregion

        // #region [lấy danh sách các subscriber]

        let list_subscriber = this.service.event_subscribers.filter(
            (x) => x.event === event
        )

        if (list_subscriber.length === 0) return result

        // #endregion

        // #region [broadcast gói tin]

        for (let subscriber of list_subscriber) {
            let result_item = { file_path: subscriber.file_path }

            try {
                await subscriber.handler({
                    service: subscriber.service,
                    _module: subscriber._module,
                    publisher,
                    args,
                })

                result_item.success = true
            } catch (e) {
                result_item.success = false
                result_item.exception = e
            }

            result.subscribers.push(result_item)
        }

        // #endregion

        // #region [xử lý kết quả phản hồi]

        result.success_count = result.subscribers.filter(
            (x) => x.success
        ).length

        result.fail_count = result.subscribers.filter((x) => !x.success).length

        return result

        // #endregion
    },
})
