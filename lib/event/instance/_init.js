let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'khởi tạo event client',
    key: 'init',
    handler: function () {
        return Instance.create({
            initial: { publishers: [], subscribers: [] },
            extensions: [require('./raise_event')],
        })
    },
})
