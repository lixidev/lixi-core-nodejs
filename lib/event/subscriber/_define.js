let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Joi = RequireModule('joi')
let Callsite = require('callsite')

module.exports = Instance.registerMethod({
    description: 'đăng ký subscriber',
    key: 'defineSub',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            event: joi.string().required().description('event sẽ lắng nghe'),
            parameter: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .default({})
                .description('tham số đầu vào'),
            handler: joi
                .func()
                .maxArity(1)
                .required()
                .description('hàm xử lý event'),
        }),
    handler: function ({ enable, event, parameter, handler }) {
        // #region [tạo instance & option]

        let subscriber = { enable, event, handler }
        subscriber.file_path = Callsite()[2].getFileName()

        // #endregion

        // #region [parameter]

        if (parameter && Object.keys(parameter).length > 0) {
            parameter = Joi.object(parameter)
            parameter = parameter.default(parameter.validate().value)
            subscriber.parameter = parameter
        }

        // #endregion

        // #region [xử lý handler]

        subscriber.handler = async function ({
            service,
            _module,
            publisher,
            args,
        }) {
            if (subscriber.parameter) {
                args = Joi.tryValidate({
                    data: args,
                    schema: subscriber.parameter,
                })
            }

            await handler({ service, _module, publisher, args })
        }

        // #endregion

        return Instance.create({
            initial: subscriber,
            extensions: [require('./load')],
        })
    },
})
