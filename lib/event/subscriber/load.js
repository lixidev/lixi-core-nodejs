let { Core } = require('../../../package')
let { Instance, EnableValue } = Core

module.exports = Instance.registerMethod({
    description: 'nạp subscriber',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp model'),
            _module: joi.object().required().description('module nạp model'),
        }),
    handler: async function ({ service, _module }) {
        // #region [enable]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })
        if (!enable) return

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        _module.event.subscribers.push(this)
        service.event_subscribers.push(this)

        // #endregion
    },
})
