let { RequireModule } = require('../../../package')
let { HttpError } = RequireModule('err')

test('khởi tạo instace', () => {
    let code = 'test'
    let http_code = 500
    let message = 'test'
    let data = { id: 1, name: 'name' }

    let result = new HttpError({ code, http_code, message, data })

    expect(result).toBeInstanceOf(Error)
    expect(result.code).toEqual(code)
    expect(result.http_code).toEqual(http_code)
    expect(result.message).toEqual(message)
    expect(result.data).toEqual(data)
})
