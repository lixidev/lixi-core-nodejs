let { RequireModule } = require('../../../package')
let { BaseError } = RequireModule('err')

test('khởi tạo instace', () => {
    let code = 'test'
    let message = 'test'
    let data = { id: 1, name: 'name' }

    let result = new BaseError({ code, message, data })

    expect(result).toBeInstanceOf(Error)
    expect(result.code).toEqual(code)
    expect(result.message).toEqual(message)
    expect(result.data).toEqual(data)
})
