let { Core } = require('../../package')
let { Instance } = Core
let Joi = require('@hapi/joi')

module.exports = Instance.registerField({
    description: 'cấu trúc lỗi',
    key: 'Schema',
    field: Joi.object({
        code: Joi.string().required().description('mã lỗi'),
        http_code: Joi.number().description('mã http status'),
        message: Joi.string().description('mô tả'),
    }),
})
