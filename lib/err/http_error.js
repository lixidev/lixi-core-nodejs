let { Core } = require('../../package')
let { Instance } = Core
let Joi = require('@hapi/joi')

module.exports = Instance.registerField({
    description: 'class lỗi HTTP',
    key: 'HttpError',
    field: class HttpError extends Error {
        constructor(args) {
            let {
                code,
                http_code,
                message,
                base_message,
                data,
                exception,
            } = Joi.attempt(
                args,
                Joi.object({
                    code: Joi.string().required().description('mã lỗi'),
                    http_code: Joi.number().description('mã http status'),
                    message: Joi.string().description('mô tả'),
                    base_message: Joi.string().description('mô tả gốc'),
                    data: Joi.any().description('dữ liệu'),
                    exception: Joi.any().description('exception gốc'),
                })
            )

            if (!message && exception) message = exception.message
            super(message)
            Error.captureStackTrace(this, this.constructor)

            this.code = code
            this.http_code = http_code
            this.base_message = base_message

            if (exception && exception.data) {
                this.data = Object.assign({}, exception.data, data)
            } else this.data = data
        }
    },
})
