let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện xử lý lỗi',
    extensions: [
        require('./base_error'),
        require('./http_error'),
        require('./schema'),
    ],
})
