let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện xử lý cấu hình',
    extensions: [require('./read_file')],
})
