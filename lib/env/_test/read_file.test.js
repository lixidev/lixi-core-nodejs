let { RequireModule } = require('../../../package')
let Env = RequireModule('env')

test('đọc file config *.env', () => {
    let result = Env.readFile({ node_mode: 'local', dir_path: __dirname })

    expect(result.NODE_ENV).toEqual('local')
    expect(result.A).toEqual('a')
    expect(result.B).toEqual(1)
    expect(result.C).toEqual(true)
})
