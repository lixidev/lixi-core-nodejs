let { Core } = require('../../package')
let { Instance } = Core
let DotEnv = require('dotenv')
let Path = require('path')

module.exports = Instance.registerMethod({
    description: 'đọc cấu hình từ file',
    key: 'readFile',
    parameter: (joi) =>
        joi.object({
            node_mode: joi
                .string()
                .required()
                .description('chế độ node sử dụng để đọc cấu hình'),
            dir_path: joi
                .string()
                .required()
                .description('đường dẫn thư mục chứa các cấu hình'),
        }),
    handler: function ({ node_mode, dir_path }) {
        // #region [nạp cấu hình]

        let path = Path.join(dir_path, node_mode + '.env')

        let env = DotEnv.config({ path }).parsed
        if (!env) {
            throw Error(`read config failed, env file "${path}" not found`)
        }

        // #endregion

        // #region [chuẩn hóa cấu hình]

        for (let key in env) {
            if (!env.hasOwnProperty(key)) continue

            switch (env[key]) {
                case 'true':
                    env[key] = true
                    break

                case 'false':
                    env[key] = false
                    break

                case 'null':
                    env[key] = null
                    break

                case 'undefined':
                    env[key] = undefined
                    break

                default:
                    if (!isNaN(env[key])) env[key] = Number(env[key])
                    break
            }
        }

        // #endregion

        env.NODE_ENV = node_mode
        return env
    },
})
