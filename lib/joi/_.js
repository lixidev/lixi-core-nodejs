let { Core } = require('../../package')
let { Instance } = Core
let Joi = require('@hapi/joi')

module.exports = Instance.create({
    description: 'thư viện validate',
    initial: Joi,
    extensions: [
        require('./type/boolean'),
        require('./type/phone'),
        require('./type/string'),
        require('./type/timestamp'),
        require('./try_validate'),
    ],
})
