let { Core } = require('../../../package')
let { Instance } = Core
let { PhoneNumberUtil, PhoneNumberFormat } = require('google-libphonenumber')

module.exports = Instance.registerExtension({
    description: 'kiểu dữ liệu số điện thoại',
    extension: function () {
        return this.extend((joi) => ({
            type: 'phone',
            base: joi.string().disallow('').meta({ baseType: 'string' }),
            messages: {
                'phone.base': '{{#label}} are invalid E.164 format',
                'phone.country': `{{#label}}'s country code must be one of {{#country}}`,
                'phone.min_subscriber':
                    '{{#label}} must have more than {{#minSN}} subscriber number',
            },
            validate(value, helpers) {
                if (!value.startsWith('+')) value = '+' + value
                let phoneUtil = PhoneNumberUtil.getInstance()
                let phone

                try {
                    phone = phoneUtil.parseAndKeepRawInput(value)
                    phone = phoneUtil.format(phone, PhoneNumberFormat.E164)
                } catch (e) {
                    return { value, errors: helpers.error('phone.base') }
                }

                return { value: phone }
            },
            rules: {
                country: {
                    multi: true,
                    method(...country) {
                        return this.$_addRule({
                            name: 'country',
                            args: { country },
                        })
                    },
                    args: [
                        {
                            name: 'country',
                            ref: true,
                            assert: (value) =>
                                Array.isArray(value) &&
                                value.every(
                                    (x) =>
                                        typeof x === 'string' &&
                                        x.length === 2 &&
                                        x === x.toUpperCase()
                                ),
                            message: 'must be a ISO 3166-1 alpha-2 codes',
                        },
                    ],
                    validate(value, helpers, args, options) {
                        let phoneUtil = PhoneNumberUtil.getInstance()

                        let phone = phoneUtil.parseAndKeepRawInput(value)
                        let country = phoneUtil.getRegionCodeForNumber(phone)
                        phone = phoneUtil.formatOutOfCountryCallingNumber(
                            phone,
                            country
                        )
                        phone = phone.replace(/[ -()]/g, '')

                        if (!args.country.includes(country)) {
                            return helpers.error('phone.country', {
                                country: args.country,
                            })
                        }

                        if (country.includes('VN') && phone.length < 10) {
                            return helpers.error('phone.min_subscriber', {
                                minSN: 10,
                            })
                        }

                        return value
                    },
                },
                minSN: {
                    method(minSN) {
                        return this.$_addRule({
                            name: 'minSN',
                            args: { minSN },
                        })
                    },
                    args: [
                        {
                            name: 'minSN',
                            ref: true,
                            assert: (value) =>
                                typeof value === 'number' && !isNaN(value),
                            message: 'must be a number',
                        },
                    ],
                    validate(value, helpers, args, options) {
                        let phoneUtil = PhoneNumberUtil.getInstance()

                        let phone = phoneUtil.parseAndKeepRawInput(value)
                        phone = phoneUtil.formatOutOfCountryCallingNumber(
                            phone,
                            phoneUtil.getRegionCodeForNumber(phone)
                        )
                        phone = phone.replace(/[ -()]/g, '')

                        if (phone.length >= args.minSN) return value
                        return helpers.error('phone.min_subscriber', {
                            minSN: args.minSN,
                        })
                    },
                },
            },
        }))
    },
})
