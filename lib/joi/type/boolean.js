let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerExtension({
    description: 'kiểu dữ liệu boolean',
    extension: function () {
        return this.extend((joi) => ({
            type: 'boolean',
            base: joi
                .alternatives()
                .try(
                    joi.boolean(),
                    joi.number(),
                    joi.string().valid(''),
                    joi.array()
                )
                .error(new Error('must be boolean or convertable boolean'))
                .meta({ baseType: 'boolean' }),
            validate(value, helpers) {
                if (value === true || value === false) return { value }

                if (typeof value === 'number') return { value: value !== 0 }

                if (value === '') return { value: false }

                if (Array.isArray(value)) return { value: value.length > 0 }
            },
        }))
    },
})
