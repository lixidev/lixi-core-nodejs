let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerExtension({
    description: 'kiểu dữ liệu string',
    extension: function () {
        return this.extend((joi) => ({
            type: 'string',
            base: joi.string().allow('').meta({ baseType: 'string' }),
            rules: {
                enum: {
                    multi: true,
                    args: [
                        {
                            name: 'values',
                            ref: true,
                            assert: (value) =>
                                typeof value === 'string' ||
                                (Array.isArray(value) &&
                                    value.every(
                                        (item) => typeof item === 'string'
                                    )),
                            message: 'must be string, array of string',
                        },
                    ],
                    method(...values) {
                        return this.disallow('').valid(...values)
                    },
                },
            },
        }))
    },
})
