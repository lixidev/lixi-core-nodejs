let { Core } = require('../../../package')
let { Instance } = Core
let Moment = require('moment-timezone')

Moment.tz.setDefault('UTC')

module.exports = Instance.registerExtension({
    description: 'kiểu dữ liệu timestamp',
    extension: function () {
        return this.extend((joi) => ({
            type: 'timestamp',
            base: joi
                .date()
                .timestamp('javascript')
                .meta({ baseType: 'number' }),
            validate(value, helpers) {
                return { value: Moment(value).valueOf() }
            },
            rules: {
                response: {
                    args: [
                        {
                            name: 'type',
                            ref: true,
                            assert: (type) =>
                                typeof type === 'string' &&
                                ['timestamp', 'date'].includes(type),
                            message: `must be ['timestamp', 'date']`,
                        },
                    ],
                    method(type) {
                        return this.$_addRule({
                            name: 'response',
                            args: { type },
                        })
                    },
                    validate(value, helpers, args, options) {
                        let { type } = args
                        switch (type) {
                            case 'date':
                                return Moment(value).toDate()
                            default:
                                return value
                        }
                    },
                },
            },
        }))
    },
})
