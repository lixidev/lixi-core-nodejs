let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')
let Helper = require('./_helper')

let schema = new Helper({ expect, schema: Joi.timestamp() })
let schemaDate = new Helper({
    expect,
    schema: Joi.timestamp().response('date'),
})

test('kiểm tra từng giá trị', () => {
    schema.checkValid({ data: undefined, expected: undefined })
    schema.checkValid({
        data: new Date(2020, 4, 5, 18, 10, 0),
        expected: 1588677000000,
    })
    schemaDate.checkValid({
        data: new Date(2020, 4, 5, 18, 10, 0),
        expected: new Date(2020, 4, 5, 18, 10, 0),
    })

    schema.checkInvalid({ expect, data: null })
})
