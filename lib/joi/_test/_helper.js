let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')

module.exports = class {
    constructor({ expect, schema }) {
        this.expect = expect
        this.schema = schema
    }

    checkValid({ data, schema, expected }) {
        if (!schema) schema = this.schema
        this.expect(Joi.tryValidate({ data, schema })).toEqual(expected)
    }

    checkInvalid({ data, schema }) {
        if (!schema) schema = this.schema
        this.expect(() => Joi.tryValidate({ data, schema })).toThrow()
    }
}
