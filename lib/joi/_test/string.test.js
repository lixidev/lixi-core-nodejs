let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')
let Helper = require('./_helper')

let schema = new Helper({ expect, schema: Joi.string() })
let schemaEnum = new Helper({ expect, schema: Joi.string().enum('a', 'b') })

test('kiểm tra từng giá trị', () => {
    schema.checkValid({ data: undefined, expected: undefined })
    schema.checkValid({ data: '', expected: '' })
    schemaEnum.checkValid({ data: 'a', expected: 'a' })

    schema.checkInvalid({ expect, data: null })
    schemaEnum.checkInvalid({ expect, data: 'c' })
})
