let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')
let Helper = require('./_helper')

let schema = new Helper({ expect, schema: Joi.object() })

test('kiểm tra từng giá trị', () => {
    schema.checkValid({ data: undefined, expected: undefined })

    schema.checkInvalid({ expect, data: null })
})
