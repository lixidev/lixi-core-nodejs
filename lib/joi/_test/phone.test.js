let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')
let Helper = require('./_helper')

let schema = new Helper({
    expect,
    schema: Joi.phone().country('VN'),
})

test('kiểm tra từng giá trị', () => {
    schema.checkValid({ data: undefined, expected: undefined })
    schema.checkValid({ data: '+84908624162', expected: '+84908624162' })
    schema.checkValid({ data: '84908624162', expected: '+84908624162' })
    schema.checkValid({ data: ' 84908624162', expected: '+84908624162' })
    schema.checkValid({ data: '+840908624162', expected: '+84908624162' })
    schema.checkValid({ data: '840908624162', expected: '+84908624162' })
    schema.checkValid({ data: ' 840908624162', expected: '+84908624162' })
    schema.checkValid({ data: '+84 0908624162', expected: '+84908624162' })
    schema.checkValid({ data: '84 0908624162', expected: '+84908624162' })
    schema.checkValid({ data: ' 84 0908624162', expected: '+84908624162' })

    schema.checkInvalid({ expect, data: null })
    schema.checkInvalid({ expect, data: '' })
    schema.checkInvalid({ expect, data: '0908624162' })
    schema.checkInvalid({ expect, data: '849086241' })
})
