let { RequireModule } = require('../../../package')
let Joi = RequireModule('joi')

let schema = Joi.object({
    id: Joi.number().default(1),
    name: Joi.string().default('test'),
    birth_date: Joi.timestamp(),
})

test('giá trị mặc định', () => {
    let now = new Date()

    let result = Joi.tryValidate({
        data: { birth_date: now },
        schema,
    })

    expect(result.id).toEqual(1)
    expect(result.name).toEqual('test')
    expect(result.birth_date).toEqual(now.getTime())
})

test('trường hợp có lỗi', () => {
    let runner = () => {
        Joi.tryValidate({
            data: { id: 'a', name: 1 },
            schema,
        })
    }

    expect(runner).toThrow()
})
