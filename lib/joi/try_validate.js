let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'kiểm tra dữ liệu',
    key: 'tryValidate',
    parameter: (joi) =>
        joi.object({
            data: joi.any().description('giá trị cần kiểm tra'),
            schema: joi
                .object()
                .schema()
                .required()
                .description('schema dùng để kiểm tra'),
            option: joi
                .object()
                .default({})
                .unknown(true)
                .description('các tham số cấu hình'),
        }),
    handler: function ({ data, schema, option }) {
        if (!option.hasOwnProperty('stripUnknown')) option.stripUnknown = true

        return this.attempt(data, schema, option)
    },
})
