let { Core } = require('../../package')
let { Instance } = Core
let Moment = require('moment-timezone')

Moment.tz.setDefault('UTC')

module.exports = Instance.create({
    description: 'thư viện xử lý thời gian',
    initial: Moment,
    extensions: [require('./format_db')],
})
