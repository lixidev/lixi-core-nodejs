let { RequireModule } = require('../../../package')
let Moment = RequireModule('moment')
let Numeral = require('numeral')

test('database format', () => {
    let now = new Date()

    let result = Moment(now).formatDb()

    expect(result).toEqual(
        now.getUTCFullYear() +
            '-' +
            Numeral(now.getUTCMonth() + 1).format('00') +
            '-' +
            Numeral(now.getUTCDate()).format('00') +
            ' ' +
            Numeral(now.getUTCHours()).format('00') +
            ':' +
            Numeral(now.getUTCMinutes()).format('00') +
            ':' +
            Numeral(now.getUTCSeconds()).format('00')
    )
})
