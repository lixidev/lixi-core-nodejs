let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerExtension({
    description: 'định dạng theo chuẩn database',
    extension: function () {
        this.fn.formatDb = function () {
            return this.format('YYYY-MM-DD HH:mm:ss')
        }
    },
})
