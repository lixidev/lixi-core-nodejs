let { Core } = require('../../package')
let { Instance } = Core
let { PhoneNumberUtil, PhoneNumberFormat } = require('google-libphonenumber')

module.exports = Instance.registerMethod({
    description: 'định dạng về lại số E164',
    key: 'formatE164',
    parameter: (joi) =>
        joi.object({
            phone: joi
                .string()
                .required()
                .description('số điện thoại cần xử lý'),
            country: joi
                .string()
                .description('mã quốc gia dùng để định dạng ISO 3166 alpha-2'),
        }),
    handler: function ({ phone, country }) {
        let phoneUtil = PhoneNumberUtil.getInstance()

        if (!country) country = this.getCountry({ phone })

        phone = phoneUtil.parseAndKeepRawInput(phone, country)
        phone = phoneUtil.format(phone, PhoneNumberFormat.E164)

        return phone
    },
})
