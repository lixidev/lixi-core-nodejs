let { Core } = require('../../package')
let { Instance } = Core
let { PhoneNumberUtil } = require('google-libphonenumber')

module.exports = Instance.registerMethod({
    description: 'định dạng về lại số quốc gia',
    key: 'formatCountry',
    parameter: (joi) =>
        joi.object({
            phone: joi
                .string()
                .required()
                .description('số điện thoại cần xử lý (format E164)'),
        }),
    handler: function ({ phone }) {
        let phoneUtil = PhoneNumberUtil.getInstance()

        let country = this.getCountry({ phone })

        phone = phoneUtil.parseAndKeepRawInput(phone)
        phone = phoneUtil.formatOutOfCountryCallingNumber(phone, country)
        phone = phone.replace(/[ -()]/g, '')

        return phone
    },
})
