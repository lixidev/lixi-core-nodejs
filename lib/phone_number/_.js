let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện xử lý số điện thoại',
    extensions: [
        require('./format_country'),
        require('./format_e164'),
        require('./get_country'),
    ],
})
