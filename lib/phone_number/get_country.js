let { Core } = require('../../package')
let { Instance } = Core
let { PhoneNumberUtil } = require('google-libphonenumber')

module.exports = Instance.registerMethod({
    description: 'lấy quốc gia của số điện thoại',
    key: 'getCountry',
    parameter: (joi) =>
        joi.object({
            phone: joi
                .string()
                .required()
                .description('số điện thoại cần xử lý (format E164)'),
        }),
    handler: function ({ phone }) {
        let phoneUtil = PhoneNumberUtil.getInstance()

        return phoneUtil.getRegionCodeForNumber(
            phoneUtil.parseAndKeepRawInput(phone)
        )
    },
})
