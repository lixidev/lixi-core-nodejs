let { RequireModule } = require('../../../package')
let PhoneNumber = RequireModule('phone_number')

test('lấy đầu số VN ở định dạnh E164', () => {
    let phone = '+84908624100'

    let result = PhoneNumber.getCountry({ phone })

    expect(result).toEqual('VN')
})
