let { RequireModule } = require('../../../package')
let PhoneNumber = RequireModule('phone_number')

test('chuyển số dạng quốc gia về số E164', () => {
    let phone = '0908624100'
    let country = 'VN'

    let result = PhoneNumber.formatE164({ phone, country })

    expect(result).toEqual('+84908624100')
})

test('chuyển số dạng E164 về số E164 (case 1)', () => {
    let phone = '+84908624100'

    let result = PhoneNumber.formatE164({ phone })

    expect(result).toEqual('+84908624100')
})

test('chuyển số dạng E164 về số E164 (case 2)', () => {
    let phone = '+840908624100'

    let result = PhoneNumber.formatE164({ phone })

    expect(result).toEqual('+84908624100')
})
