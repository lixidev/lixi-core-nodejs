let { RequireModule } = require('../../../package')
let PhoneNumber = RequireModule('phone_number')

test('chuyển số dạng E164 về số quốc gia', () => {
    let phone = '+84908624100'

    let result = PhoneNumber.formatCountry({ phone })

    expect(result).toEqual('0908624100')
})
