/** tính toán load order của các module */

module.exports = function ({ service }) {
    for (let _module of service.modules) {
        _module.load_order = calculateLoadOrderModule({ service, _module })
    }

    service.modules.sort((a, b) => a.load_order - b.load_order)
}

function calculateLoadOrderModule({ service, _module }) {
    let load_order = 0

    for (let dependency of _module.dependencies) {
        if (dependency.type === 'node_module') continue

        let dependency_module = service.modules.find(
            (x) => x.name === dependency.name
        )

        let dependency_load_order = calculateLoadOrderModule({
            service,
            _module: dependency_module,
        })

        if (load_order <= dependency_load_order) {
            load_order = dependency_load_order + 1
        }
    }

    return load_order
}
