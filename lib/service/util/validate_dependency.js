/** validate các dependency của module */

module.exports = function ({ service }) {
    for (let _module of service.modules) {
        for (let dependency of _module.dependencies) {
            let error_message = 'module "' + _module.name + '"'

            if (dependency.type === 'module') {
                let validate_result = service.modules.some((x) => {
                    let check_name = x.name === dependency.name

                    let check_version
                    if (!dependency.version) check_version = true
                    else Semver.satisfies(x.version, dependency.version)

                    return check_name && check_version
                })
                if (validate_result) continue

                error_message +=
                    'required module "' +
                    dependency.name +
                    (dependency.version ? '@' + dependency.version : '') +
                    '"'
            }

            if (dependency.optional) {
                console.warn('\x1b[33m[WARNING]: %s\x1b[0m', error_message)
            } else throw Error('service start failed, ' + error_message)
        }
    }
}
