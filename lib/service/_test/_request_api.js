let { RequireModule } = require('../../../package')
let { HttpError } = RequireModule('err')
let Axios = require('axios')

module.exports = async function ({
    service,
    method,
    path,
    header,
    query,
    body,
}) {
    try {
        let response = await Axios({
            method: method,
            url: `http://localhost:${service.api_server.option.port}${path}`,
            headers: header,
            params: query,
            data: body,
        })

        return response.data
    } catch (e) {
        if (e.response && e.response.data && e.response.data.error) {
            throw new HttpError({
                code: e.response.data.error.code,
                http_code: e.response.status,
                message: e.response.data.error.message,
                data: e.response.data.error.data,
                exception: e,
            })
        } else if (e.response && e.response.data) {
            throw new HttpError({
                code: 'exception',
                http_code: e.response.status,
                message: e.response.data,
                exception: e,
            })
        } else throw e
    }
}
