let RequestApi = require('./_request_api')

module.exports = [
    {
        test: 'calculator - case 1',
        handler: async ({ service }) => {
            let { data } = await RequestApi({
                service,
                method: 'post',
                path: '/calculator',
                body: {
                    a: 6,
                    b: 3,
                },
            })

            expect(data.a).toEqual(6)
            expect(data.b).toEqual(3)
            expect(data.is_a_undefined).toEqual(false)
            expect(data.is_b_undefined).toEqual(false)
            expect(data.sum).toEqual(9)
            expect(data.sub).toEqual(3)
            expect(data.mul).toEqual(18)
            expect(data.div).toEqual(2)
            expect(data.error).toEqual(null)
        },
    },
    {
        test: 'calculator - case 2',
        handler: async ({ service }) => {
            let error

            try {
                await RequestApi({
                    service,
                    method: 'post',
                    path: '/calculator',
                    body: {
                        a: 6,
                        b: 0,
                    },
                })
            } catch (e) {
                error = e
            }

            expect(error).toBeDefined()
        },
    },
    {
        test: 'calculator - case 3',
        handler: async ({ service }) => {
            let { data } = await RequestApi({
                service,
                method: 'post',
                path: '/calculator',
                body: {
                    a: null,
                },
            })

            expect(data.a).toEqual(null)
            expect(data.b).toEqual(null)
            expect(data.is_a_undefined).toEqual(false)
            expect(data.is_b_undefined).toEqual(true)
            expect(data.sum).toEqual(null)
            expect(data.sub).toEqual(null)
            expect(data.mul).toEqual(null)
            expect(data.div).toEqual(null)
            expect(data.error).toEqual(null)
        },
    },
]
