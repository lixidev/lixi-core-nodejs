let RequestApi = require('./_request_api')

module.exports = [
    {
        test: 'hello',
        handler: async ({ service }) => {
            let { data } = await RequestApi({
                service,
                method: 'get',
                path: '/hello',
                query: {
                    names: ['TripleSix'],
                },
            })

            expect(data.text).toEqual('hello TripleSix')
        },
    },
    {
        test: 'hello (error case)',
        handler: async ({ service }) => {
            let error

            try {
                await RequestApi({
                    service,
                    method: 'get',
                    path: '/hello',
                })
            } catch (e) {
                error = e
            }

            expect(error.http_code).toEqual(400)
            expect(error.message).toEqual('"parameter.names" is required')
        },
    },
]
