let RequestApi = require('./_request_api')

module.exports = [
    {
        test: 'list paging',
        handler: async ({ service }) => {
            let { meta, data } = await RequestApi({
                service,
                method: 'get',
                path: '/list/paging',
            })

            expect(meta.total).toEqual(20)
            expect(meta.page_count).toEqual(2)
            expect(data).toHaveLength(10)
        },
    },
    {
        test: 'list paging, page_size 5, page 2',
        handler: async ({ service }) => {
            let { meta, data } = await RequestApi({
                service,
                method: 'get',
                path: '/list/paging',
                query: {
                    page: 2,
                    page_size: 5,
                },
            })

            expect(meta.total).toEqual(20)
            expect(meta.page_count).toEqual(4)
            expect(data).toHaveLength(5)
        },
    },
]
