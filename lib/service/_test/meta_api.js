let RequestApi = require('./_request_api')

module.exports = [
    {
        test: 'not found',
        handler: async ({ service }) => {
            let error
            try {
                await RequestApi({
                    service,
                    method: 'get',
                    path: '/not/found',
                })
            } catch (e) {
                error = e
            }

            expect(error.http_code).toEqual(404)
        },
    },
    {
        test: '_meta/healthcheck',
        handler: async ({ service }) => {
            let data = await RequestApi({
                service,
                method: 'get',
                path: '/_meta/healthcheck',
            })

            expect(data).toEqual(`I'm still alive`)
        },
    },
    {
        test: '_meta/info',
        handler: async ({ service }) => {
            let data = await RequestApi({
                service,
                method: 'get',
                path: '/_meta/info',
            })

            expect(data.version).toEqual('test')
        },
    },
]
