let testcases = [].concat(
    require('./calculator_api'),
    require('./hello_api'),
    require('./list_api'),
    require('./meta_api'),
    require('./upload_api')
)

let service
beforeAll(async () => {
    service = await require('../../../sample/service')()
    await service.start()
})

for (let testcase of testcases) {
    test(testcase.test, async () => await testcase.handler({ service }))
}
