let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tắt service',
    key: 'stop',
    handler: async function () {
        await this.api_server.stop()
        if (this.document) await this.document.stop()
    },
})
