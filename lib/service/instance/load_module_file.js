let { Core } = require('../../../package')
let { Instance } = Core
let Fs = require('fs')

module.exports = Instance.registerMethod({
    description: 'nạp module từ file',
    key: 'loadModuleFile',
    parameter: (joi) =>
        joi.object({
            path: joi
                .string()
                .required()
                .description('đường dẫn đến module cần nạp'),
        }),
    handler: async function ({ path }) {
        if (!Fs.existsSync(path)) {
            throw Error(`load module failed, "${path}" - file not found`)
        }

        let _module = require(path)
        await _module.load({ service: this })
    },
})
