let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'lấy module',
    key: 'getModule',
    parameter: (joi) =>
        joi.object({
            name: joi.string().required().description('tên module cần lấy'),
        }),
    handler: function ({ name }) {
        return this.modules.find((x) => x.name === name)
    },
})
