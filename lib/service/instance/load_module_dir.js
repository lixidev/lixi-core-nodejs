let { Core } = require('../../../package')
let { Instance } = Core
let Fs = require('fs')
let Path = require('path')

module.exports = Instance.registerMethod({
    description: 'nạp tất cả module trong thư mục',
    key: 'loadModuleDir',
    parameter: (joi) =>
        joi.object({
            dir_path: joi
                .string()
                .required()
                .description('đường dẫn đến thư mục chứa các module'),
        }),
    handler: async function ({ dir_path }) {
        if (!Fs.existsSync(dir_path)) {
            throw Error(`load module failed, "${dir_path}" - folder not found`)
        }

        let dir_names = Fs.readdirSync(dir_path)
        for (let dir_name of dir_names) {
            await this.loadModuleFile({
                path: Path.join(dir_path, dir_name),
            })
        }
    },
})
