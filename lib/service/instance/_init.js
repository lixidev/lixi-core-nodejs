let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Logger = RequireModule('logger')
let Model = RequireModule('model')

module.exports = Instance.registerMethod({
    description: 'khởi tạo server instance',
    key: 'init',
    parameter: (joi) =>
        joi.object({
            root_dir_path: joi
                .string()
                .required()
                .description('đường dẫn gốc của project'),
            show_log_on_start: joi
                .boolean()
                .default(true)
                .description('hiển thị log khi service khởi động'),
            show_log_on_error: joi
                .boolean()
                .default(true)
                .description('hiển thị log khi có lỗi'),
            api_server: joi
                .object()
                .required()
                .description('api server sử dụng cho service'),
            logger: joi.object().description('logger sử dụng cho service'),
            document: joi.object().description('document sử dụng cho service'),
            option: joi.object().description('thiết lập mở rộng'),
        }),
    handler: function ({
        root_dir_path,
        show_log_on_start,
        show_log_on_error,
        api_server,
        logger,
        document,
        option,
    }) {
        if (!logger) logger = Logger.init()

        if (!option) option = {}
        option = Object.assign(option, {
            root_dir_path,
            show_log_on_start,
            show_log_on_error,
        })

        let service = {
            option,
            api_server,
            logger,
            document,
            event_subscribers: [],
            modules: [],
            model: Model.init(),
        }

        service.api_server.service = service
        if (service.document) service.document.service = service

        return Instance.create({
            initial: service,
            extensions: [
                require('./get_module'),
                require('./load_module_dir'),
                require('./load_module_file'),
                require('./start'),
                require('./stop'),
            ],
        })
    },
})
