let { Core } = require('../../../package')
let { Instance } = Core

let ValidateDependency = require('../util/validate_dependency')
let CalculateLoadOrder = require('../util/calculate_load_order')

module.exports = Instance.registerMethod({
    description: 'khởi chạy service',
    key: 'start',
    handler: async function () {
        // #region [validate]

        CalculateLoadOrder({ service: this })
        ValidateDependency({ service: this })

        for (let _module of this.modules) {
            await _module.validate()
        }

        // #endregion

        // #region [chạy hàm khởi động của module]

        for (let _module of this.modules) {
            if (!_module.on_start) continue
            await _module.on_start({ service: this, _module })
        }

        // #endregion

        // #region [khởi động api server]

        await this.api_server.start()

        if (this.option.show_log_on_start) {
            let server_address
            if (this.api_server.option.public_domain) {
                server_address = this.api_server.option.public_domain
            } else {
                server_address =
                    this.api_server.option.protocol +
                    '://' +
                    this.api_server.option.host +
                    ':' +
                    this.api_server.option.port
            }

            this.logger.info('api server has started: ' + server_address)
        }

        // #endregion

        // #region [khởi động document server]

        if (this.document) {
            try {
                await this.document.start()

                if (this.option.show_log_on_start) {
                    let server_address
                    if (this.document.option.public_domain) {
                        server_address = this.document.option.public_domain
                    } else {
                        server_address =
                            this.document.option.protocol +
                            '://' +
                            this.document.option.host +
                            ':' +
                            this.document.option.port
                    }

                    this.logger.info(
                        'document server has started: ' + server_address
                    )
                }
            } catch (e) {
                e.message = 'document server start failed, ' + e.message
                this.logger.error(e)
            }
        }

        // #endregion
    },
})
