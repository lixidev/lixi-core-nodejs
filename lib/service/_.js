let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện xử lý service',
    extensions: [require('./instance/_init')],
})
