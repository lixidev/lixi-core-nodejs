let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện migrate',
    extensions: [require('./define'), require('./knex_migration')],
})
