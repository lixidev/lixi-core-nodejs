let { Core, RequireModule } = require('../../package')
let { Instance } = Core
let Knex = RequireModule('knex')

module.exports = Instance.registerMethod({
    description: 'đăng ký migration',
    key: 'define',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            input: joi
                .func()
                .arity(0)
                .description('hàm xử lý input của migration'),
            up: joi
                .func()
                .maxArity(1)
                .required()
                .description('hàm thực thi migrate'),
            down: joi
                .func()
                .maxArity(1)
                .required()
                .description('hàm rollback migrate'),
        }),
    handler: function ({ enable, input, up, down }) {
        let migration = { enable, input }
        migration.option = {}

        migration.up = async (knex) => {
            let input = await prepareInput({ migration, knex })
            return await up.bind(migration)(input)
        }

        migration.down = async (knex) => {
            let input = await prepareInput({ migration, knex })
            return await down.bind(migration)(input)
        }

        return migration
    },
})

async function prepareInput({ migration, knex }) {
    let input = {}

    if (migration.input) input = await migration.input.bind(migration)()
    input.db = Knex.init({ knex })

    return input
}
