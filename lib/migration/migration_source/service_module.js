let { Core } = require('../../../package')
let { EnableValue, ListAllFile } = Core
let Path = require('path')
let Fs = require('fs')

module.exports = class ServiceModuleMigrationSource {
    constructor({ service }) {
        this.service = service
        this.option = {}
    }

    async getMigrations() {
        let result = []

        for (let _module of this.service.modules) {
            for (let dir_name of _module.migration_dir_names) {
                let migration_dir_path = Path.join(_module.file_path, dir_name)
                if (!Fs.existsSync(migration_dir_path)) continue

                let migrations = []
                let file_paths = ListAllFile({
                    path: migration_dir_path,
                    extnames: ['.js'],
                })
                for (let file_path of file_paths) {
                    let migration = require(file_path)
                    migration.option.service = this.service
                    Object.assign(migration.option, this.option)

                    let enable = await EnableValue({
                        target: migration,
                        input: { service: this.service },
                    })
                    if (!enable) continue

                    migrations.push({
                        file_path,
                        file_name: Path.basename(file_path),
                        name: _module.name + '/' + Path.basename(file_path),
                    })
                }

                migrations = migrations.sort((a, b) =>
                    a.file_name.localeCompare(b.file_name)
                )
                result = result.concat(migrations)
            }
        }

        return result
    }

    getMigrationName(input) {
        return input.name
    }

    getMigration(input) {
        let migration = require(input.file_path)
        migration.option.service = this.service
        Object.assign(migration.option, this.option)

        return migration
    }
}
