let { Core, RequireModule } = require('../../package')
let { Instance } = Core
let Knex = RequireModule('knex')

let ServiceModuleMigrationSource = require('./migration_source/service_module')

module.exports = Instance.registerMethod({
    description: 'knex migration source',
    key: 'knexMigration',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service instance'),
            database: joi
                .any()
                .description('thông tin kết nối database, theo hàm Knex.init'),
            validate_migration_list: joi
                .boolean()
                .default(false)
                .description('có kiểm tra danh sách migration hay không?'),
        }),
    handler: function ({ service, database, validate_migration_list }) {
        let knex = Knex.init(database)
        let input = {
            migrationSource: new ServiceModuleMigrationSource({ service }),
            disableMigrationsListValidation: !validate_migration_list,
        }

        return {
            latest: () => knex.client.migrate.latest(input),
            rollback: () => knex.client.migrate.rollback(input, false),
            rollbackAll: () => knex.client.migrate.rollback(input, true),
            up: (name) =>
                knex.client.migrate.up(Object.assign({ name }, input)),
            down: (name) =>
                knex.client.migrate.down(Object.assign({ name }, input)),
        }
    },
})
