let { RequireModule } = require('../../../package')
let Knex = RequireModule('knex')

test('init', () => {
    let db = Knex.init(require('./_config'))

    expect(db.schema.constructor.name).toEqual('SchemaBuilder')
})
