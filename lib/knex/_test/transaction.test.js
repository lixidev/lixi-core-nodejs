let { RequireModule } = require('../../../package')
let Knex = RequireModule('knex')

test('init', () => {
    let db = Knex.init(require('./_config'))

    let result = db.transaction({ handler: (trx) => {} })

    expect(result.constructor.name).toEqual('Transaction_MySQL')
})
