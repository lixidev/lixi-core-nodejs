let { RequireModule } = require('../../../package')
let Knex = RequireModule('knex')

test('init', () => {
    let db = Knex.init(require('./_config'))

    let result = db.query()

    expect(result.constructor.name).toEqual('Builder')
})
