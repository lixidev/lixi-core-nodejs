let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'thực hiện câu truy vấn insert hoặc update 1 record',
    key: 'insertOrUpdate',
    parameter: (joi) =>
        joi.object({
            insert: joi
                .object()
                .required()
                .description('dữ liệu dùng khi tạo record'),
            update: joi
                .object()
                .required()
                .description('dữ liệu dùng khi cập nhật record'),
        }),
    handler: async function ({ insert, update }) {
        let result = await this.client.raw(
            this.clone().insert(insert).toString() +
                ' on duplicate key update ' +
                this.clone()
                    .update(update)
                    .toString()
                    .substr(14 + this._single.table.length)
        )

        result = result[0]
        result.submit_mode = result.affectedRows > 1 ? 'update' : 'insert'

        return result
    },
})
