let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tạo câu truy vấn',
    key: 'query',
    handler: function () {
        let query = this.client.queryBuilder()
        query.db = this

        return Instance.create({
            initial: query,
            extensions: [require('./insert_or_update')],
        })
    },
})
