let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerProperty({
    description: 'thao tác với cấu trúc bảng dữ liệu',
    key: 'schema',
    get: function () {
        return this.client.schema
    },
})
