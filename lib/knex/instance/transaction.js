let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tạo transaction truy vấn',
    key: 'transaction',
    parameter: (joi) =>
        joi.object({
            handler: joi
                .func()
                .arity(1)
                .required()
                .description('hàm xử lý transaction'),
        }),
    handler: function ({ handler }) {
        return this.client.transaction(async (trx) => {
            trx.option = this.option
            trx = Instance.create({
                description: 'knex transaction',
                initial: trx,
                extensions: [require('../query/_init')],
            })

            return await handler(trx)
        })
    },
})
