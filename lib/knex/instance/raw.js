let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerExtension({
    description: 'truy vấn raw',
    key: 'raw',
    extension: function () {
        return this.client.raw.bind(this.client)
    },
})
