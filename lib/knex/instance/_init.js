let { Core } = require('../../../package')
let { Instance } = Core
let Knex = require('knex')

module.exports = Instance.registerMethod({
    description: 'khởi tạo knex client',
    key: 'init',
    parameter: (joi) =>
        joi
            .object({
                knex: joi.any().description('knex client'),
                dialect: joi
                    .string()
                    .valid('oracle', 'mysql', 'mssql', 'pg', 'sqlite3')
                    .description('hệ database'),
                connection: joi
                    .object({
                        host: joi.string().description('host của database'),
                        port: joi
                            .number()
                            .port()
                            .description('port của database'),
                        username: joi
                            .string()
                            .description('tên đăng nhập vào database'),
                        password: joi
                            .string()
                            .description('mật khẩu đăng nhập vào database'),
                        database: joi.string().description('database thao tác'),
                        filename: joi
                            .string()
                            .description('đường dẫn đến database'),
                        timezone: joi
                            .string()
                            .default('utc')
                            .description('timezone thao tác với database'),
                        charset: joi
                            .string()
                            .description('charset thao tác với database'),
                    })
                    .when('dialect', [
                        {
                            is: 'oracle',
                            then: joi
                                .object({
                                    host: joi.required(),
                                    port: joi.required(),
                                    username: joi.required(),
                                    password: joi.required(),
                                    database: joi.required(),
                                })
                                .required(),
                        },
                        {
                            is: 'mysql',
                            then: joi
                                .object({
                                    host: joi.required(),
                                    port: joi.required(),
                                    username: joi.required(),
                                    password: joi.required(),
                                    database: joi.required(),
                                })
                                .required(),
                        },
                        {
                            is: 'mssql',
                            then: joi
                                .object({
                                    host: joi.required(),
                                    port: joi.required(),
                                    username: joi.required(),
                                    password: joi.required(),
                                    database: joi.required(),
                                })
                                .required(),
                        },
                        {
                            is: 'pg',
                            then: joi
                                .object({
                                    host: joi.required(),
                                    port: joi.required(),
                                    username: joi.required(),
                                    password: joi.required(),
                                    database: joi.required(),
                                })
                                .required(),
                        },
                        {
                            is: 'sqlite3',
                            then: joi
                                .object({
                                    filename: joi.required(),
                                })
                                .required(),
                        },
                    ])
                    .description('thông tin kết nối database'),
                option: joi
                    .object()
                    .unknown(true)
                    .default({})
                    .description('các thiết lập mở rộng'),
            })
            .or('knex', 'dialect'),
    handler: function ({ knex, dialect, connection, option }) {
        let db = {}

        if (knex) {
            let client = knex.connection().client
            db.option = {
                dialect: client.config.client,
                connection: client.config.connection,
            }
            db.client = knex
        } else {
            db.option = {
                dialect,
                connection,
            }

            if (
                ['oracle', 'mysql', 'mssql', 'pg'].includes(db.option.dialect)
            ) {
                if (!db.option.connection.timezone) {
                    if (['oracle', 'mssql', 'pg'].includes(db.option.dialect)) {
                        db.option.connection.timezone = 'utf8'
                    } else db.option.connection.timezone = 'utf8mb4'
                }

                db.client = Knex(
                    Object.assign(option, {
                        client: db.option.dialect,
                        connection: {
                            host: db.option.connection.host,
                            port: db.option.connection.port,
                            user: db.option.connection.username,
                            password: db.option.connection.password,
                            database: db.option.connection.database,
                            timezone: db.option.connection.timezone,
                            charset: db.option.connection.charset,
                        },
                    })
                )
            } else if (['sqlite3'].includes(db.option.dialect)) {
                db.client = Knex(
                    Object.assign(option, {
                        client: db.option.dialect,
                        connection: {
                            filename: db.option.connection.filename,
                            timezone: db.option.connection.timezone,
                            charset: db.option.connection.charset,
                        },
                        pool: { min: 0, max: 10 }
                    })
                )
            }
        }

        return Instance.create({
            initial: db,
            extensions: [
                require('../query/_init'),
                require('./raw'),
                require('./schema'),
                require('./transaction'),
            ],
        })
    },
})
