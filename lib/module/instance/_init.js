let { Core, RequireModule } = require('../../../package')
let { Instance, MakeKey } = Core
let Event = RequireModule('event')
let Hub = RequireModule('hub')
let Func = RequireModule('func')
let Callsite = require('callsite')
let Semver = require('semver')
let Path = require('path')

module.exports = Instance.registerMethod({
    description: 'khởi tạo module instance',
    key: 'init',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt module'),
            name: joi.string().required().description('tên module'),
            version: joi
                .string()
                .required()
                .description('phiên bản của module'),
            author: joi.string().description('tác giả'),
            description: joi.string().description('mô tả về module'),
            dependencies: joi
                .array()
                .items(
                    joi.object({
                        type: joi
                            .string()
                            .valid('module')
                            .default('module')
                            .description('loại dependency'),
                        name: joi.string().required().description('tên gọi'),
                        version: joi.string().description('phiên bản yêu cầu'),
                        optional: joi
                            .boolean()
                            .default(false)
                            .description('có bắt buộc phải có hay không?'),
                    })
                )
                .default([])
                .description('danh sách dependency yêu cầu'),
            migration_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['migration'])
                .description('các thư mục chứa migration'),
            event_publisher_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['event_pub'])
                .description('các thư mục chứa event publisher'),
            event_subscriber_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['event_sub'])
                .description('các thư mục chứa event subscriber'),
            hub_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['hub'])
                .description('các thư mục chứa hub'),
            api_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['api'])
                .description('các thư mục chứa api'),
            function_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['function', 'biz', 'core'])
                .description('các thư mục chứa function'),
            model_dir_names: joi
                .array()
                .items(joi.string())
                .min(1)
                .default(['model'])
                .description('các thư mục chứa model schema'),

            error_require_path: joi
                .string()
                .default('error')
                .description('đường dẫn đến nơi khai báo các mã lỗi'),
            on_start: joi
                .func()
                .maxArity(1)
                .description('hàm xử lý khi module được khởi động'),
        }),
    handler: function ({
        enable,
        name,
        version,
        author,
        description,
        dependencies,
        migration_dir_names,
        event_publisher_dir_names,
        event_subscriber_dir_names,
        hub_dir_names,
        api_dir_names,
        function_dir_names,
        model_dir_names,
        error_require_path,
        on_start,
    }) {
        // #region [validate]

        if (!Semver.valid(version)) throw Error('module version is invalid')

        // #endregion

        // #region [tạo instance & option]

        let _module = {
            enable,
            name,
            version,
            description,
            author,
            dependencies,
            migration_dir_names,
            event_publisher_dir_names,
            event_subscriber_dir_names,
            hub_dir_names,
            api_dir_names,
            function_dir_names,
            model_dir_names,
            error_require_path,
            on_start,
            event: Event.init(),
            hub: Hub.init(),
            func: Func.init(),
            apis: [],
            models: [],
        }

        _module.key = MakeKey({ text: name })
        _module.file_path = Path.dirname(Callsite()[2].getFileName())

        // #endregion

        return Instance.create({
            initial: _module,
            extensions: [
                require('./load'),
                require('./validate'),
                require('./get_model_schema'),
                require('./get_model'),
                require('./run_func'),
            ],
        })
    },
})
