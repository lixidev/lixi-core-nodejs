let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'validate module',
    key: 'validate',
    handler: async function () {
        await this.hub.validate()
    },
})
