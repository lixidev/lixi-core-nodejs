let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'chạy function',
    key: 'runFunc',
    parameter: (joi) =>
        joi.object({
            name: joi.string().required().description('tên function cần lấy'),
            type: joi
                .string()
                .allow(null)
                .default(null)
                .description('loại function cần lấy'),
            args: joi.any().description('tham số đầu vào'),
        }),
    handler: async function ({ name, type, args }) {
        return await this.func.runFunc({ name, type, args })
    },
})
