let { Core, RequireModule } = require('../../../package')
let { Instance, EnableValue, CheckExists, ListAllFile } = Core
let Err = RequireModule('err')
let Joi = RequireModule('joi')
let Path = require('path')
let Fs = require('fs')

module.exports = Instance.registerMethod({
    description: 'nạp module',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp module'),
        }),
    handler: async function ({ service }) {
        let file_paths

        // #region [enable & exists]

        let enable = await EnableValue({
            target: this,
            input: { service, _module: this },
        })
        if (!enable) return

        CheckExists({
            item: this,
            list: service.modules,
            error_message: 'load module failed',
        })

        // #endregion

        // #region [nạp model schema]

        for (let dir_name of this.model_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let model = require(file_path)
                    await model.load({ service, _module: this })
                } catch (e) {
                    e.message = `model file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp event event publisher]

        for (let dir_name of this.event_publisher_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let eventPub = require(file_path)
                    await eventPub.load({ service, _module: this })
                } catch (e) {
                    e.message = `event publisher file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp event event subscriber]

        for (let dir_name of this.event_subscriber_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let eventSub = require(file_path)
                    await eventSub.load({ service, _module: this })
                } catch (e) {
                    e.message = `event subscriber file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp hub]

        for (let dir_name of this.hub_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let hub = require(file_path)
                    await hub.load({ service, _module: this })
                } catch (e) {
                    e.message = `hub file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp api]

        for (let dir_name of this.api_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let api = require(file_path)
                    await api.load({ service, _module: this })
                } catch (e) {
                    e.message = `api file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp function]

        for (let dir_name of this.function_dir_names) {
            let dir_path = Path.join(this.file_path, dir_name)

            file_paths = ListAllFile({ path: dir_path })
            for (let file_path of file_paths) {
                try {
                    let func = require(file_path)
                    if (['biz', 'core'].includes(dir_name)) {
                        func.type = dir_name
                    }

                    await func.load({ service, _module: this })
                } catch (e) {
                    e.message = `function file invalid, "${file_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [nạp các mã lỗi]

        let error_require_path = Path.join(
            this.file_path,
            this.error_require_path
        )

        if (Fs.existsSync(error_require_path + '.js')) {
            error_require_path += '.js'
        } else if (
            Fs.existsSync(error_require_path) &&
            Fs.lstatSync(error_require_path).isDirectory()
        ) {
            error_require_path = Path.join(error_require_path, 'index.js')
        }

        if (Fs.existsSync(error_require_path)) {
            let list_error = require(error_require_path)
            if (list_error) {
                try {
                    list_error = Joi.tryValidate({
                        data: list_error,
                        schema: Joi.array().items(Err.Schema),
                    })

                    for (let error of list_error) {
                        service.api_server.registerError(error)
                    }
                } catch (e) {
                    e.message = `error file invalid, "${error_require_path}", ${e.message}`
                    throw e
                }
            }
        }

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this.event.service = service
        service.modules.push(this)

        // #endregion
    },
})
