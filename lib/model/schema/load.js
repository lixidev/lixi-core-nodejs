let { Core } = require('../../../package')
let { Instance, EnableValue, CheckExists } = Core

module.exports = Instance.registerMethod({
    description: 'nạp model schema',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp model'),
            _module: joi.object().required().description('module nạp model'),
        }),
    handler: async function ({ service, _module }) {
        // #region [field value]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })

        // #endregion

        // #region [enable & exists]

        if (!enable) return
        CheckExists({
            item: this,
            list: service.model.schemas,
            error_message: 'load model failed',
        })

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        service.model.schemas.push(this)
        _module.models.push(this)

        // #endregion

        // #region [raise event]

        service.model.raiseEvent({
            event: 'model_loaded',
            data: this,
        })

        // #endregion
    },
})
