let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Joi = RequireModule('joi')

module.exports = Instance.registerProperty({
    description: 'joi của model schema',
    key: 'joi_schema',
    get: function () {
        return Joi.object(this.fields)
    },
})
