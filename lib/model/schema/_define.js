let { Core } = require('../../../package')
let { Instance, FileInfo, MakeKey } = Core

module.exports = Instance.registerMethod({
    description: 'khai báo model schema',
    key: 'defineSchema',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi.string().description('tên gọi'),
            description: joi.string().description('mô tả'),
            fields: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .required()
                .description('danh sách các field'),
            field_links: joi
                .object()
                .pattern(
                    joi.string().description('field dùng để liên kết'),
                    joi.object({
                        model: joi
                            .string()
                            .required()
                            .description('model liên kết'),
                        field: joi
                            .string()
                            .required()
                            .description('field liên kết tới'),
                    })
                )
                .description('danh sách các field liên kết'),
        }),
    handler: function ({ enable, name, description, fields, field_links }) {
        let model = { enable, name, description, fields, field_links }

        // #region [chuẩn bị dữ liệu]

        let file_info = FileInfo()
        if (!model.name) model.name = file_info.name
        model.key = MakeKey({ text: model.name })
        model.file_path = file_info.path

        // #endregion

        // #region [create instance]

        return Instance.create({
            initial: model,
            extensions: [require('./load'), require('./joi_schema')],
        })

        // #endregion
    },
})
