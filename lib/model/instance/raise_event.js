let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'phát sinh sự kiện',
    key: 'raiseEvent',
    parameter: (joi) =>
        joi.object({
            event: joi.string().required().description('tên event'),
            data: joi.object().description('thông tin event'),
        }),
    handler: async function ({ event, data }) {
        let ev = this.events[event]
        if (!ev) return

        for (let handler of ev.handlers) {
            await handler(data)
        }
    },
})
