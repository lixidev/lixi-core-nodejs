let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'xóa xử lý sự kiện',
    key: 'clearEvent',
    parameter: (joi) =>
        joi.object({
            event: joi.string().required().description('tên event'),
        }),
    handler: async function ({ event }) {
        delete this.events[event]
    },
})
