let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'đăng ký xử lý cho sự kiện',
    key: 'onEvent',
    parameter: (joi) =>
        joi.object({
            event: joi.string().required().description('tên event'),
            handler: joi
                .func()
                .maxArity(1)
                .required()
                .description('hàm xử lý sự kiện'),
        }),
    handler: async function ({ event, handler }) {
        let ev = this.events[event]

        if (!ev) this.events[event] = { handlers: [handler] }
        else ev.handlers.push(handler)
    },
})
