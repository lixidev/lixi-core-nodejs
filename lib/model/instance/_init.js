let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'khởi tạo model instance',
    key: 'init',
    handler: function () {
        return Instance.create({
            initial: {
                schemas: [],
                events: {},
            },
            extensions: [
                require('./get_model_schema'),
                require('./get_model'),
                require('./on_event'),
                require('./clear_event'),
                require('./raise_event'),
            ],
        })
    },
})
