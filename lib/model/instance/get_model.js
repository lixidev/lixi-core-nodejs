let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'lấy model',
    key: 'getModel',
    parameter: (joi) =>
        joi.object({
            name: joi
                .string()
                .required()
                .description('tên model schema cần lấy'),
        }),
    handler: function ({ name }) {
        return this.schemas.find((x) => x.name === name)
    },
})
