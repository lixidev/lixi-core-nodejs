let { Core, RequireModule } = require('../../../package')
let { Instance } = Core
let Joi = RequireModule('joi')

module.exports = Instance.registerMethod({
    description: 'lấy model schema',
    key: 'getModelSchema',
    parameter: (joi) =>
        joi.object({
            name: joi
                .string()
                .required()
                .description('tên model schema cần lấy'),
        }),
    handler: function ({ name }) {
        let schema = this.schemas.find((x) => x.name === name)
        if (!schema) throw new Error(`model '${name}' not found`)

        return Joi.object(schema.fields)
    },
})
