let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện xử lý model',
    extensions: [require('./instance/_init'), require('./schema/_define')],
})
