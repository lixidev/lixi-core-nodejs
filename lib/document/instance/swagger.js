let { Core } = require('../../../package')
let { Instance, Location } = Core
let Joi2Swagger = require('joi-to-swagger')

module.exports = Instance.registerMethod({
    description: 'tạo dữ liệu swagge',
    key: 'swagger',
    handler: async function () {
        let { data, service } = this
        let { api_server } = service

        let result = {
            swagger: '2.0',
            info: {},
            paths: {},
        }

        if (data.name) {
            result.info.title = data.name + ' - Swagger'
        } else {
            result.info.title = 'API Swagger'
        }

        if (api_server.option.public_domain) {
            result.host = api_server.option.public_domain
        } else {
            result.host = api_server.option.host
            if (api_server.option.port) {
                result.host += ':' + api_server.option.port
            }
        }
        result.basePath = '/'
        result.schemes = [api_server.option.protocol]

        for (let api of api_server.apis) {
            // #region [create end-point]

            let path = api.path
            if (path.includes(':')) {
                path = path.split('/')
                for (let i = 0; i < path.length; i++) {
                    if (path[i].startsWith(':')) {
                        path[i] = '{' + path[i].substr(1) + '}'
                    }
                }
                path = path.join('/')
            }

            if (!result.paths.hasOwnProperty(path)) result.paths[path] = {}
            if (!result.paths[path].hasOwnProperty(api.method)) {
                result.paths[path][api.method] = {}
            }

            let endpoint = result.paths[path][api.method]

            // #endregion

            // #region [basic info]

            endpoint.summary = api.description
            endpoint.consumes = ['application/json']
            endpoint.produces = ['application/json']

            // #endregion

            // #region [parameter]

            endpoint.parameters = []
            let body = {
                name: 'body',
                in: 'body',
                required: true,
                schema: {
                    type: 'object',
                    required: [],
                    properties: {},
                },
            }

            for (let key in api.parameter) {
                if (!api.parameter.hasOwnProperty(key)) continue
                let schema = api.parameter[key].describe()

                let location
                if (schema.metas) {
                    location = schema.metas.find((x) =>
                        x.hasOwnProperty('location')
                    )
                }

                if (location) location = location.location
                else {
                    location = Location.default({ method: api.method })
                }

                if (['path', 'query', 'header'].includes(location)) {
                    let parameter = Joi2Swagger(api.parameter[key]).swagger
                    if (schema.flags) parameter.default = schema.flags.default
                    parameter.example = parameter.default
                    parameter.name = key
                    parameter.in = location

                    if (schema.flags && schema.flags.presence === 'required') {
                        parameter.required = true
                    }
                    endpoint.parameters.push(parameter)
                } else if (location === 'body') {
                    let parameter = Joi2Swagger(api.parameter[key]).swagger
                    if (schema.flags) parameter.default = schema.flags.default
                    parameter.example = parameter.default

                    body.schema.properties[key] = parameter
                    if (schema.flags && schema.flags.presence === 'required') {
                        body.schema.required.push(key)
                    }
                }
            }

            if (Object.keys(body.schema.properties).length > 0) {
                endpoint.parameters.push(body)
            }

            // #endregion

            // #region [lấy thông tin các field]

            let { meta_field, data_field, error_field } = api.workflow.find(
                (x) => x.key === 'prepare'
            ).option

            // #endregion

            // #region [middleware file_upload]

            middleware_file_upload = api.workflow.find(
                (x) => x.key === 'file_upload'
            )
            if (middleware_file_upload) {
                endpoint.parameters.push({
                    type: 'file',
                    name: middleware_file_upload.option.field,
                    in: 'formData',
                    description: 'file upload',
                })
            }

            // #endregion

            // #region [success response]

            let successResponse = {}
            endpoint.responses = {
                '200': {
                    schema: {
                        type: 'object',
                        properties: successResponse,
                    },
                },
            }

            successResponse[meta_field] = {
                type: 'object',
                description: 'các thông tin mở rộng',
                properties: {
                    success: {
                        type: 'boolean',
                        description:
                            'phiên request được xử lý thành công hay thất bại?',
                        default: true,
                    },
                    request_uuid: {
                        type: 'string',
                        format: 'uuid',
                        description: 'mã định danh phiên request',
                    },
                    request_time: {
                        type: 'number',
                        format: 'integer',
                        description: 'thời gian tiếp nhận request (timestamp)',
                    },
                    response_time: {
                        type: 'number',
                        format: 'integer',
                        description: 'thời gian phản hồi request (timestamp)',
                    },
                    execution_time: {
                        type: 'number',
                        format: 'float',
                        // eslint-disable-next-line max-len
                        description:
                            'thời gian server tiến hành xử lý của phiên request (đơn vị millisecond)',
                    },
                },
            }

            if (api.response) {
                if (api.response.describe().type === 'any') {
                    successResponse[data_field] = {
                        type: 'any',
                    }
                } else {
                    successResponse[data_field] = Joi2Swagger(
                        api.response
                    ).swagger
                }
            } else {
                successResponse[data_field] = {
                    type: 'boolean',
                    default: true,
                }
            }

            successResponse[data_field].description = 'kết quả xử lý'

            // #endregion

            // #region [error response]

            endpoint.responses['error'] = {
                schema: {
                    type: 'object',
                    properties: {
                        [meta_field]: {
                            type: 'object',
                            description: 'các thông tin mở rộng',
                            properties: {
                                success: {
                                    type: 'boolean',
                                    description:
                                        'phiên request được xử lý thành công hay thất bại?',
                                    default: false,
                                },
                                request_uuid: {
                                    type: 'string',
                                    format: 'uuid',
                                    description: 'mã định danh phiên request',
                                },
                                request_time: {
                                    type: 'number',
                                    format: 'integer',
                                    description:
                                        'thời gian tiếp nhận request (timestamp)',
                                },
                                response_time: {
                                    type: 'number',
                                    format: 'integer',
                                    description:
                                        'thời gian phản hồi request (timestamp)',
                                },
                                execution_time: {
                                    type: 'number',
                                    format: 'float',
                                    // eslint-disable-next-line max-len
                                    description:
                                        'thời gian server tiến hành xử lý của phiên request (đơn vị millisecond)',
                                },
                            },
                        },
                        [error_field]: {
                            type: 'object',
                            description: 'thông tin chi tiết lỗi',
                            properties: {
                                code: {
                                    type: 'string',
                                    description: 'mã định danh lỗi',
                                },
                                message: {
                                    type: 'string',
                                    description: 'mô tả lỗi',
                                },
                                data: {
                                    type: 'any',
                                    description: 'dữ liệu hỗ trợ xử lý lỗi lỗi',
                                },
                            },
                        },
                    },
                },
            }

            if (api_server.option.show_error_stack) {
                endpoint.responses['error'].schema.properties[
                    error_field
                ].properties.stack = {
                    type: 'any',
                    description: 'stack hỗ trợ truy vết lỗi',
                }
            }

            // #endregion
        }

        return result
    },
})
