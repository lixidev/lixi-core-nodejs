let { Core } = require('../../../package')
let { Instance } = Core
let Express = require('express')
let Path = require('path')

let MiddlewareBasicAuth = require('../util/middleware_basic_auth')
// let RouteWikiDetail = require('../util/route_wiki_detail');

module.exports = Instance.registerMethod({
    description: 'khởi tạo document instance',
    key: 'init',
    parameter: (joi) =>
        joi.object({
            bitbucket_slug: joi
                .string()
                .description('tên project trên bitbucket'),
            bitbucket_branch: joi
                .string()
                .description('nhánh của project trên bitbucket'),
            protocol: joi
                .string()
                .default('http')
                .description('protocol sử dụng cho api'),
            host: joi
                .string()
                .default('localhost')
                .description('host sử dụng cho api'),
            port: joi
                .number()
                .default(8081)
                .description('port sử dụng cho api'),
            public_domain: joi
                .string()
                .description('public domain để vào document'),
            auth: joi
                .object({
                    mode: joi
                        .string()
                        .valid('none', 'basic')
                        .required()
                        .description('loại xác thực'),
                    basic_list_account: joi
                        .array()
                        .items(
                            joi.object({
                                username: joi
                                    .string()
                                    .description('tên đăng nhập')
                                    .required(),
                                password: joi
                                    .string()
                                    .description('mật khẩu')
                                    .required(),
                            })
                        )
                        .description('danh sách tài khoản dùng cho basic auth'),
                })
                .default({ mode: 'none' })
                .description('cấu hình bảo mật document'),
        }),
    handler: function ({
        bitbucket_slug,
        bitbucket_branch,
        protocol,
        host,
        port,
        public_domain,
        auth,
    }) {
        // #region [validate]

        if (auth.mode === 'basic' && auth.basic_list_account.length <= 0) {
            throw Error('must have at least 1 account for basic auth')
        }

        // #endregion

        // #region [khởi tạo]

        let document = {
            data: {},
            option: {
                bitbucket_slug,
                bitbucket_branch,
                protocol,
                host,
                port,
                public_domain,
                auth,
            },
        }

        // #endregion

        // #region [cài đặt server]

        document.server = Express()

        if (auth.mode === 'basic') {
            document.server.use((req, res, next) =>
                MiddlewareBasicAuth({ document, req, res, next })
            )
        }

        document.server.use(
            '/',
            Express.static(Path.join(__dirname, '../../../site/document-ui'))
        )

        document.server.use(
            '/swagger',
            Express.static(Path.join(__dirname, '../../../site/swagger-ui'))
        )

        document.server.get('/document.json', (req, res) =>
            res.send(document.data)
        )

        // document.server.get('/wiki/:key.md', (req, res) =>
        //     RouteWikiDetail({ document, req, res })
        // );

        document.server.get('/swagger/data.json', async (req, res) => {
            try {
                let result = await document.swagger()
                res.send(result)
            } catch (e) {
                res.status(500)
                res.send(e.message)
                throw e
            }
        })

        document.server.get('/*', (req, res) =>
            res.sendFile(
                Path.join(__dirname, '../../../site/document-ui/index.html')
            )
        )

        // #endregion

        // #region [tạo instance]

        return Instance.create({
            initial: document,
            extensions: [
                require('./start'),
                require('./stop'),
                require('./swagger'),
            ],
        })

        // #endregion
    },
})
