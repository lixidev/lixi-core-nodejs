let { Core } = require('../../../package')
let { Instance } = Core

let LoadInfo = require('../util/load_info')
let LoadApiServer = require('../util/load_api_server')
let LoadModule = require('../util/load_module')
let LoadApiEndpoint = require('../util/load_api_endpoint')
let LoadModel = require('../util/load_model')

module.exports = Instance.registerMethod({
    description: 'khởi chạy server',
    key: 'start',
    handler: async function () {
        if (this._instance) return

        if (!this.is_initialized) {
            await LoadInfo({ document: this })
            await LoadApiServer({ document: this })
            await LoadModule({ document: this })
            await LoadApiEndpoint({ document: this })
            await LoadModel({ document: this })

            this.is_initialized = true
        }

        this._instance = this.server.listen(this.option.port)
    },
})
