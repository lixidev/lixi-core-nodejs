let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'tắt server',
    key: 'stop',
    handler: function () {
        if (!this._instance) return

        this._instance.close()
        delete this._instance
    },
})
