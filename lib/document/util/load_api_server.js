module.exports = async function ({ document }) {
    let { service, data } = document
    data.api_server = {}

    if (service.api_server.option.public_domain) {
        data.api_server.address = service.api_server.option.public_domain
    } else {
        data.api_server.address =
            service.api_server.option.protocol +
            '://' +
            service.api_server.option.host +
            ':' +
            service.api_server.option.port
    }

    data.api_server.list_version = service.api_server.list_version
    data.api_server.default_version = service.api_server.option.default_version
}
