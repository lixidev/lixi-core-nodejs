let { Core } = require('../../../package')
let { CalculateCodeSize, Location } = Core

module.exports = async function ({ document }) {
    let { service, data } = document

    data.apis = []
    for (let _module of service.modules) {
        for (let api of _module.apis) {
            let item = ApiItem({ api })

            data.modules.find((x) => x.key === _module.key).apis.push(item.key)
            data.apis.push(item)
        }
    }
}

function ApiItem({ api }) {
    let result = {
        key: api.key,
        file_path: api.file_path,
        name: api.name,
        description: api.description,
        version: api.version,
        method: api.method,
        path: api.path,
        middlewares: api.workflow.map((x) => ({
            key: x.key,
            name: x.name,
            option: x.option,
        })),
    }

    // #region [parameters]

    result.parameters = []
    for (let key in api.parameter) {
        if (!api.parameter.hasOwnProperty(key)) continue
        let schema = api.parameter[key].describe()

        // #region [location]

        let location
        if (schema.metas) {
            location = schema.metas.find((x) => x.hasOwnProperty('location'))
        }

        if (location) location = location.location
        else location = Location.default({ method: api.method })

        // #endregion

        let parameter = { key, location }
        readSchema({ result: parameter, schema })

        result.parameters.push(parameter)
    }

    // #endregion

    // #region [response]

    if (api.response) {
        result.response = readSchema({ schema: api.response.describe() })
    }

    // #endregion

    // #region [code size]

    result.code_size = CalculateCodeSize({ file_path: api.file_path })

    // #endregion

    // #region [module khai báo]

    result.modules = [api._module.key]

    // #endregion

    return result
}

function readSchema({ result, schema }) {
    if (!result) result = {}

    result.type = schema.type

    result.description = schema.description

    result.default = schema.flags ? schema.flags.default : undefined

    result.required = false
    if (schema.flags && schema.flags.presence === 'required') {
        result.required = true
    }

    result.valids = schema.valids

    if (schema.invalids) {
        let invalids = schema.invalids.filter(
            (x) => ![Infinity, -Infinity].includes(x)
        )
        result.invalids = invalids.length > 0 ? invalids : undefined
    }

    let min
    if (schema.rules) min = schema.rules.find((x) => x.name === 'min')
    if (min) result.min = min.arg

    let max
    if (schema.rules) max = schema.rules.find((x) => x.name === 'max')
    if (max) result.max = max.arg

    if (schema.type === 'array') {
        result.item_type = readSchema({ schema: schema.items[0] })
    }

    if (schema.type === 'object') {
        result.children = {}
        for (let key in schema.children) {
            if (!schema.children.hasOwnProperty(key)) continue
            result.children[key] = readSchema({ schema: schema.children[key] })
        }

        if (Object.keys(result.children).length === 0) delete result.children
    }

    return result
}
