let { Core } = require('../../../package')
let { CalculateCodeSize } = Core

module.exports = async function ({ document }) {
    let { service, data } = document

    data.models = []
    for (let _module of service.modules) {
        for (let model of _module.models) {
            let item = ModelItem({ model })

            data.modules
                .find((x) => x.key === _module.key)
                .models.push(item.key)
            data.models.push(item)
        }
    }
}

function ModelItem({ model }) {
    let result = {
        key: model.key,
        file_path: model.file_path,
        name: model.name,
        description: model.description,
        fields: [],
        field_link: model.field_link,
    }

    // #region [fields]

    for (let key in model.fields) {
        if (!model.fields.hasOwnProperty(key)) continue
        let schema = model.fields[key].describe()
        let field = {}

        field.name = key
        if (schema.flags) field.description = schema.flags.description
        field.type = schema.type
        field.allow_null = !!(schema.allow && schema.allow.includes(null))

        if (schema.flags && schema.flags.default) {
            field.default = schema.flags.default
        } else if (field.allow_null) field.default = null

        let enum_values
        if (schema.allow) {
            enum_values = schema.allow.filter((x) => ![null, ''].includes(x))
        }
        if (enum_values && enum_values.length > 0) field.enum = enum_values

        let min
        if (schema.rules) min = schema.rules.find((x) => x.name === 'min')
        if (min) field.min = min.args.limit

        let max
        if (schema.rules) max = schema.rules.find((x) => x.name === 'max')
        if (max) field.max = max.args.limit

        result.fields.push(field)
    }

    // #endregion

    // #region [code size]

    result.code_size = CalculateCodeSize({ file_path: model.file_path })

    // #endregion

    // #region [module khai báo]

    result.modules = [model._module.key]

    // #endregion

    return result
}
