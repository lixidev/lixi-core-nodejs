let BasicAuth = require('basic-auth')

module.exports = function ({ document, req, res, next }) {
    let account = BasicAuth(req)
    let is_valid = !!document.option.auth.basic_list_account.find((x) => {
        return (
            account &&
            x.username === account.name &&
            x.password === account.pass
        )
    })

    if (!is_valid) {
        res.set('WWW-Authenticate', 'Basic realm="example"')
        res.status(401).send()
        return
    }

    return next()
}
