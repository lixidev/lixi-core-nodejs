let { Core } = require('../../../package')
let { CalculateCodeSize, ListAllFile } = Core

module.exports = async function ({ document }) {
    let { service, data } = document

    data.modules = []
    for (let _module of service.modules) {
        let item = ModuleItem({ _module })
        data.modules.push(item)
    }
}

function ModuleItem({ _module }) {
    let result = {
        key: _module.key,
        file_path: _module.file_path,
        name: _module.name,
        description: _module.description,
        version: _module.version,
        author: _module.author,
        list_dependency: _module.dependencies.map((x) => ({
            type: x.type,
            name: x.name,
            version: x.version,
        })),
        event_pubs: [],
        event_subs: [],
        hubs: [],
        apis: [],
        funcs: [],
        models: [],
        wikis: [],
    }

    // #region [code size]

    result.code_size = 0
    let file_paths = ListAllFile({ path: _module.file_path, extnames: '.js' })
    for (let file_path of file_paths) {
        result.code_size += CalculateCodeSize({ file_path })
    }

    // #endregion

    return result
}
