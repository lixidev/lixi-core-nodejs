/** request đến api của bitbucket */

let Axios = require('axios')

module.exports = async function ({ auth, url, param }) {
    return (
        await Axios({
            timeout: 5000,
            url: 'https://bitbucket.org/api/2.0' + url,
            params: Object.assign(
                {
                    access_token: auth.access_token,
                },
                param
            ),
        })
    ).data
}
