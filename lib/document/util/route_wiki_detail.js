let Fs = require('fs')

module.exports = function ({ document, req, res }) {
    // #region [tìm wiki]

    let wiki
    if (document.data.wikis) {
        wiki = document.data.wikis.find((x) => x.key === req.params.key)
    }

    if (!wiki) {
        res.status(404).send('wiki "' + req.params.key + '" not found')
        return
    }

    // #endregion

    // #region [cắt bỏ metadata]

    let file_content = Fs.readFileSync(wiki.file_path, 'utf8')
    file_content = file_content.substring(file_content.indexOf('}-->') + 4)

    // #endregion

    // #region [response]

    res.set('Content-Type', 'text/markdown')
    res.set('charset', 'UTF-8')
    res.send(file_content)

    // #endregion
}
