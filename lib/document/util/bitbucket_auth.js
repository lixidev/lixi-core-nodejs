/** phát sinh access token của bitbucket */

let Axios = require('axios')
let OAuth = require('axios-oauth-client')

module.exports = async function () {
    let client = OAuth.client(Axios.create({ timeout: 5000 }), {
        url: 'https://bitbucket.org/site/oauth2/access_token',
        grant_type: 'client_credentials',
        client_id: 'tJf6sbw9k6cABeXmzG',
        client_secret: 'dfTeNGD79MdpZ4RYveZhpm8YHu6b45Am',
    })

    return await client()
}
