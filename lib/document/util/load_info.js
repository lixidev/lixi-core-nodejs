let { Core, RequireModule } = require('../../../package')
let { Info } = Core
let Moment = RequireModule('moment')

let BitbucketAuth = require('./bitbucket_auth')
let BitbucketRequest = require('./bitbucket_request')

module.exports = async function ({ document }) {
    let { service, data } = document
    let auth
    let repository
    let repository_last_commit

    // #region [lấy thông tin từ bitbucket]

    try {
        auth = await BitbucketAuth()

        repository = await BitbucketRequest({
            auth,
            url: `/repositories/${document.option.bitbucket_slug}`,
        })

        repository_last_commit = await BitbucketRequest({
            auth,
            url: `/repositories/${document.option.bitbucket_slug}/commits/${document.option.bitbucket_branch}`,
        })

        repository_last_commit = repository_last_commit.values[0]
    } catch (e) {
        if (service.option.show_log_on_error) {
            service.logger.warn(
                'document server: cannot load infomation from bitbucket'
            )
        }
    }

    // #endregion

    data.name = repository ? repository.name : 'service'
    data.description = repository ? repository.description : undefined
    data.source_code_link =
        'https://bitbucket.org/' + document.option.bitbucket_slug
    if (repository_last_commit) {
        data.last_update_datetime = Moment(
            repository_last_commit.date
        ).valueOf()
    }
    data.core_framework = Info
}
