let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'validate',
    key: 'validate',
    handler: async function () {
        for (let endpoint of this._endpoints) {
            await endpoint.validate()
        }
    },
})
