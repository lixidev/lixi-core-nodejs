let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'khởi tạo hub instance',
    key: 'init',
    handler: function () {
        return Instance.create({
            initial: { _endpoints: [] },
            extensions: [require('./validate')],
        })
    },
})
