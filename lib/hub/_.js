let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện gửi và nhận dữ liệu',
    extensions: [
        require('./instance/_init'),
        require('./endpoint/_define_api'),
        require('./endpoint/_define_func'),
        require('./endpoint/_define_require'),
    ],
})
