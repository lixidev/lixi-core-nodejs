let { Core } = require('../../../package')
let { Instance, EnableValue, CheckExists } = Core

module.exports = Instance.registerMethod({
    description: 'nạp hub end-point',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp model'),
            _module: joi.object().required().description('module nạp model'),
        }),
    handler: async function ({ service, _module }) {
        // #region [field value]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })

        // #endregion

        // #region [enable & exists]

        if (!enable) return
        CheckExists({
            item: this,
            list: _module.hub._endpoints,
            error_message: 'load end-point failed',
        })

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        _module.hub[this.name] = this.handler.bind(this)
        _module.hub._endpoints.push(this)

        // #endregion
    },
})
