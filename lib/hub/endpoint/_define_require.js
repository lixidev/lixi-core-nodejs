let { Core } = require('../../../package')
let { Instance, FileInfo, MakeKey } = Core
let Callsite = require('callsite')
let Path = require('path')

let invalid_name = require('../util/invalid_name')

module.exports = Instance.registerMethod({
    description: 'khai báo end-point file',
    key: 'defineRequire',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi.string().description('tên gọi').invalid(invalid_name),
            file_path: joi
                .string()
                .required()
                .description('đường dẫn đến file code'),
        }),
    handler: function ({ enable, name, file_path }) {
        let endpoint = { type: 'require', enable, name }

        // #region [require path]

        endpoint.require_path = file_path
        if (endpoint.require_path.startsWith('.')) {
            endpoint.require_path = Path.join(
                Path.dirname(Callsite()[2].getFileName()),
                endpoint.require_path
            )
        }

        // #endregion

        // #region [chuẩn bị dữ liệu]

        let file_info = FileInfo()
        if (!endpoint.name) endpoint.name = file_info.name
        endpoint.key = MakeKey({ text: endpoint.name })
        endpoint.file_path = file_info.path

        // #endregion

        // #region [handler]

        endpoint.handler = async function () {
            return require(this.require_path)
        }

        // #endregion

        return Instance.create({
            initial: endpoint,
            extensions: require('./_extension'),
        })
    },
})
