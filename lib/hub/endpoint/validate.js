let { Core } = require('../../../package')
let { Instance } = Core
let Fs = require('fs')

module.exports = Instance.registerMethod({
    description: 'validate',
    key: 'validate',
    handler: async function () {
        if (!validators.hasOwnProperty(this.type)) return

        await validators[this.type].bind(this)()
    },
})

let validators = {
    require: async function () {
        if (!Fs.existsSync(this.file_path)) {
            throw Error(
                `hub "${this.name}" error: file path "${this.file_path}" not found`
            )
        }
    },

    func: async function () {
        let service = this.service.getModule({ name: this.module_name })
        if (!service) {
            throw Error(
                `hub "${this.name}" error: module "${this.module_name}" not found`
            )
        }

        if (
            !service.func.getFunc({
                name: this.func_name,
                type: this.func_type,
            })
        ) {
            let func_name =
                (this.func_type ? '(' + this.func_type + ')' : '') +
                this.func_name

            throw Error(
                `hub "${this.name}" error: function "${func_name}" not found`
            )
        }
    },
}
