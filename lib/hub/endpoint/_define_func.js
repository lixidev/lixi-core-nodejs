let { Core } = require('../../../package')
let { Instance, FileInfo, MakeKey } = Core

let invalid_name = require('../util/invalid_name')

module.exports = Instance.registerMethod({
    description: 'khai báo end-point func',
    key: 'defineFunc',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi.string().description('tên gọi').invalid(invalid_name),
            module_name: joi.string().required().description('tên module'),
            func_name: joi.string().required().description('tên function'),
            func_type: joi
                .string()
                .allow(null)
                .default(null)
                .description('loại function'),
        }),
    handler: function ({ enable, name, module_name, func_name, func_type }) {
        let endpoint = {
            type: 'func',
            enable,
            name,
            module_name,
            func_name,
            func_type,
        }

        // #region [chuẩn bị dữ liệu]

        let file_info = FileInfo()
        if (!endpoint.name) endpoint.name = file_info.name
        endpoint.key = MakeKey({ text: endpoint.name })
        endpoint.file_path = file_info.path

        // #endregion

        // #region [handler]

        endpoint.handler = async function (input) {
            return await this.service
                .getModule({ name: this.module_name })
                .func.runFunc({
                    name: this.func_name,
                    type: this.func_type,
                    args: input ? input.args : undefined,
                })
        }

        // #endregion

        return Instance.create({
            initial: endpoint,
            extensions: require('./_extension'),
        })
    },
})
