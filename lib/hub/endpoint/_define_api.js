let { Core } = require('../../../package')
let { Instance, FileInfo, MakeKey } = Core
let Axios = require('axios')

let invalid_name = require('../util/invalid_name')

module.exports = Instance.registerMethod({
    description: 'khai báo end-point file',
    key: 'defineApi',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi.string().description('tên gọi').invalid(invalid_name),
            host: joi.string().required().description('host sẽ request'),
            path: joi
                .string()
                .regex(/^\//, '"must starts with /"')
                .required()
                .description('path sẽ request'),
            method: joi
                .string()
                .valid('get', 'post', 'put', 'delete')
                .required()
                .description('http method sử dụng'),
            header: joi
                .object()
                .pattern(joi.string(), joi.string())
                .description('các giá trị header mặc định'),
            response_data_field: joi
                .string()
                .default('data')
                .description('field chứa dữ liệu trong response'),
            response_error_field: joi
                .string()
                .default('error')
                .description('field chứa lỗi trong response'),
        }),
    handler: function ({
        enable,
        name,
        host,
        path,
        method,
        header,
        response_data_field,
        response_error_field,
    }) {
        let endpoint = {
            type: 'api',
            enable,
            name,
            host,
            path,
            method,
            header,
            response_data_field,
            response_error_field,
        }

        // #region [chuẩn bị dữ liệu]

        let file_info = FileInfo()
        if (!endpoint.name) endpoint.name = file_info.name
        endpoint.key = MakeKey({ text: endpoint.name })
        endpoint.file_path = file_info.path

        // #endregion

        // #region [client]

        endpoint.client = Axios.create({
            baseURL: host + path,
            headers: header || {},
        })

        // #endregion

        // #region [handler]

        endpoint.handler = async function ({ args, header }) {
            // #region [chuẩn bị gói tin]

            let input = {
                method: this.method,
                headers: Object.assign({}, this.header, header),
            }

            if (input.method === 'get') input.params = args
            else input.data = args

            // #endregion

            try {
                let response = (await this.client.request(input)).data
                return response[this.response_data_field]
            } catch (e) {
                let err
                if (
                    e.response &&
                    e.response.data &&
                    e.response.data[this.response_error_field]
                ) {
                    err = e.response.data[this.response_error_field]
                } else if (e.response && e.response.data) err = e.response.data
                else if (e.response) err = e.response
                else err = e

                let http_code =
                    e.response && e.response.status ? e.response.status : 500

                throw new HttpError({
                    code: err.code || 'exception',
                    http_code,
                    mesasge: err.mesasge || err.msg,
                    data: err.data || err,
                    exception: e,
                })
            }
        }

        // #endregion

        return Instance.create({
            initial: endpoint,
            extensions: require('./_extension'),
        })
    },
})
