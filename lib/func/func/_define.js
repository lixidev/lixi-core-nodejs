let { Core } = require('../../../package')
let { Instance, FileInfo } = Core

module.exports = Instance.registerMethod({
    description: 'khai báo function',
    key: 'defineFunc',
    parameter: (joi) =>
        joi.object({
            enable: joi
                .alternatives()
                .try(joi.boolean(), joi.func().maxArity(1))
                .default(true)
                .description('bật/tắt'),
            name: joi.string().description('tên gọi'),
            type: joi
                .string()
                .allow(null)
                .default(null)
                .description('loại hàm'),
            parameter: joi
                .object()
                .pattern(joi.string(), joi.object().schema())
                .default({})
                .description('tham số đầu vào'),
            response: joi
                .alternatives()
                .try(joi.object().schema(), joi.func().maxArity(1))
                .description('kết quả trả về'),
            handler: joi.func().maxArity(1).description('hàm xử lý'),
        }),
    handler: function ({ enable, name, type, parameter, response, handler }) {
        let func = { enable, name, type, parameter, response, handler }

        // #region [chuẩn bị dữ liệu]

        let file_info = FileInfo()
        if (!func.name) func.name = file_info.name
        func.file_path = file_info.path

        // #endregion

        // #region [create instance]

        return Instance.create({
            initial: func,
            extensions: [require('./load')],
        })

        // #endregion
    },
})
