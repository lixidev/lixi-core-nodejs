let { Core, RequireModule } = require('../../../package')
let {
    Instance,
    EnableValue,
    ParameterValue,
    ResponseValue,
    MakeKey,
    CheckExists,
} = Core
let Joi = RequireModule('joi')

module.exports = Instance.registerMethod({
    description: 'nạp function',
    key: 'load',
    parameter: (joi) =>
        joi.object({
            service: joi.object().required().description('service nạp model'),
            _module: joi.object().required().description('module nạp model'),
        }),
    handler: async function ({ service, _module }) {
        // #region [field value]

        let enable = await EnableValue({
            target: this,
            input: { service, _module },
        })
        let parameter = await ParameterValue({
            target: this,
            input: { service, _module },
        })
        let response = await ResponseValue({
            target: this,
            input: { service, _module },
        })

        // #endregion

        // #region [cập nhật key]

        this.key = MakeKey({
            text: (this.type ? this.type + '_' : '') + this.name,
        })

        // #endregion

        // #region [enable & exists]

        if (!enable) return
        CheckExists({
            item: this,
            list: _module.func.functions,
            error_message: 'load function failed',
        })

        // #endregion

        // #region [xử lý handler]

        let handler = this.handler
        this.handler = async function ({ service, _module, args }) {
            if (parameter) {
                args = Joi.tryValidate({
                    data: args,
                    schema: parameter,
                })
            }

            let result = await handler({ service, _module, args })

            if (response) {
                result = Joi.tryValidate({
                    data: result,
                    schema: response,
                })
            }

            return result
        }

        // #endregion

        // #region [ghi nhận]

        this.service = service
        this._module = _module
        _module.func.functions.push(this)

        // #endregion
    },
})
