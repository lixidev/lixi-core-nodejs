let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện quản lý hàm, chức năng',
    extensions: [require('./instance/_init'), require('./func/_define')],
})
