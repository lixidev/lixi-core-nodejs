let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'lấy function',
    key: 'getFunc',
    parameter: (joi) =>
        joi.object({
            name: joi.string().required().description('tên function cần lấy'),
            type: joi
                .string()
                .allow(null)
                .default(null)
                .description('loại function cần lấy'),
        }),
    handler: function ({ name, type }) {
        return this.functions.find((x) => x.name === name && x.type === type)
    },
})
