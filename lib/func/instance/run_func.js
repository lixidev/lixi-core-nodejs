let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'chạy function',
    key: 'runFunc',
    parameter: (joi) =>
        joi.object({
            name: joi.string().required().description('tên function cần lấy'),
            type: joi
                .string()
                .allow(null)
                .default(null)
                .description('loại function cần lấy'),
            args: joi.any().description('tham số đầu vào'),
        }),
    handler: async function ({ name, type, args }) {
        let func = this.getFunc({ name, type })
        if (!func) {
            let func_name = (type ? '(' + type + ')' : '') + name
            throw Error(`function "${func_name}" is not found`)
        }

        return await func.handler({
            service: func.service,
            _module: func._module,
            args,
        })
    },
})
