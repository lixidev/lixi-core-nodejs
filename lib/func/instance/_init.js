let { Core } = require('../../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'khởi tạo model instance',
    key: 'init',
    handler: function () {
        return Instance.create({
            initial: { functions: [] },
            extensions: [require('./get_func'), require('./run_func')],
        })
    },
})
