let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'giả mã chuỗi base64',
    key: 'base64Decode',
    parameter: (joi) =>
        joi.object({
            data: joi
                .string()
                .invalid('')
                .required()
                .description('chuỗi base64 cần xử lý'),
        }),
    handler: function ({ data }) {
        let buff = new Buffer(data, 'base64')
        return buff.toString('ascii')
    },
})
