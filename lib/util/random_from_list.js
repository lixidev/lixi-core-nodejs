let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'phát sinh ngẫu nhiên từ danh sách',
    key: 'randomFromList',
    parameter: (joi) =>
        joi.object({
            list: joi
                .array()
                .min(1)
                .required()
                .description('danh sách các dữ liệu dùng để ngẫu nhiên'),
        }),
    handler: function ({ list }) {
        return list[Math.floor(Math.random() * list.length)]
    },
})
