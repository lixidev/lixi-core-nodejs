let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'mã hóa chuỗi thành base64',
    key: 'base64Encode',
    parameter: (joi) =>
        joi.object({
            text: joi
                .string()
                .invalid('')
                .required()
                .description('chuỗi cần xử lý'),
        }),
    handler: function ({ text }) {
        let buff = new Buffer(text)
        return buff.toString('base64')
    },
})
