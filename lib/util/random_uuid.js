let { Core } = require('../../package')
let { Instance } = Core
let { v4: UuidV4 } = require('uuid')

module.exports = Instance.registerMethod({
    description: 'phát sinh chuỗi uuid ngẫu nhiên',
    key: 'randomUuid',
    parameter: (joi) =>
        joi.object({
            include_separator: joi
                .boolean()
                .default(true)
                .description(
                    'chuỗi phát sinh có kèm ký tự phân cách "-" hay không?'
                ),
        }),
    handler: function ({ include_separator }) {
        let result = UuidV4()
        if (!include_separator) result = result.replace(/-/g, '')

        return result
    },
})
