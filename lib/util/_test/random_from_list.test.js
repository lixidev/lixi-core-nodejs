let { RequireModule } = require('../../../package')
let Util = RequireModule('util')

test('phát sinh ngẫu nhiên', () => {
    let list = [1, 2, 3, 4, 5]
    let result = Util.randomFromList({ list })

    expect(list).toContain(result)
})
