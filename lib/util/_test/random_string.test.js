let { RequireModule } = require('../../../package')
let Util = RequireModule('util')

test('phát sinh chuỗi 100 ký tự', () => {
    let length = 100
    let result = Util.randomString({ length })

    expect(result).toHaveLength(length)
})

test('phát sinh chuỗi với các ký tự chỉ định', () => {
    let length = 1000
    let source = '12345'
    let result = Util.randomString({ length, source })

    expect(result).toHaveLength(length)
    for (let i = 0; i < length; i++) {
        expect(source).toContain(result[i])
    }
})
