let { RequireModule } = require('../../../package')
let Util = RequireModule('util')
let Joi = RequireModule('joi')

test('phát sinh chuỗi uuid', () => {
    let result = Util.randomUuid()

    Joi.assert(result, Joi.string().uuid())
})

test('phát sinh chuỗi uuid không có dấu cách', () => {
    let result = Util.randomUuid({ include_separator: false })

    expect(result).toHaveLength(32)
    expect(result).not.toContain('-')
})
