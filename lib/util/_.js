let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện hỗ trợ linh tinh',
    extensions: [
        require('./base64_decode'),
        require('./base64_encode'),
        require('./random_from_list'),
        require('./random_string'),
        require('./random_uuid'),
    ],
})
