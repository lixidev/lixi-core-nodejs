let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.registerMethod({
    description: 'phát sinh chuỗi ngẫu nhiên',
    key: 'randomString',
    parameter: (joi) =>
        joi.object({
            length: joi
                .number()
                .required()
                .description('độ dài chuỗi sẽ phát sinh'),
            source: joi
                .string()
                .default(
                    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                )
                .description('chuỗi ký tự sử dụng để phát sinh'),
        }),
    handler: function ({ length, source }) {
        let result = ''

        for (let i = 0; i < length; i++) {
            result += source[Math.floor(Math.random() * source.length)]
        }

        return result
    },
})
