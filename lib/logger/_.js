let { Core } = require('../../package')
let { Instance } = Core

module.exports = Instance.create({
    description: 'thư viện log',
    extensions: [require('./instance/_init')],
})
