let { Core } = require('../../../package')
let { Instance } = Core
let Winston = require('winston')

module.exports = Instance.registerMethod({
    description: 'khởi tạo logger',
    key: 'init',
    parameter: (joi) =>
        joi.object({
            level: joi
                .string()
                .valid('error', 'warn', 'info', 'verbose', 'debug', 'silly')
                .default('info')
                .description('mức độ ghi nhận log'),
            show_error_stack: joi
                .boolean()
                .default(true)
                .description('có hiển thị slack chi tiết lỗi hay không?'),
        }),
    handler: function ({ level, show_error_stack }) {
        let logger = Winston.createLogger({
            level,
            format: Winston.format.combine(
                Winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                Winston.format.errors({ stack: show_error_stack }),
                Winston.format.splat(),
                Winston.format.json()
            ),
        })

        logger.add(
            new Winston.transports.Console({
                format: Winston.format.combine(
                    Winston.format.colorize(),
                    Winston.format.simple()
                ),
            })
        )

        return logger
    },
})
