let { RequireModule } = require('../../../package')
let Logger = RequireModule('logger')

test('khởi tạo logger', () => {
    let logger = Logger.init()

    expect(logger.constructor.name).toEqual('DerivedLogger')
})
