(async () => {
    let service = await require('./index')({
        show_log_on_start: true,
        show_log_on_error: true,
    });
    await service.start();
})();
