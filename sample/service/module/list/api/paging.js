let { Api, Joi } = require('../../../../../index');

module.exports = Api.defineEndpoint({
    method: 'get',
    path: '/list/paging',
    response: Joi.array().items(
        Joi.object({
            id: Joi.number(),
            name: Joi.string(),
        })
    ),
    middlewares: [{ name: 'paging', enable: true }],
    handler: async function ({ args }) {
        let result = [];
        let start = (args.page - 1) * args.page_size;
        let end = Math.min(start + args.page_size, data_items.length) - 1;
        for (let i = start; i <= end; i++) {
            result.push(data_items[i]);
        }

        return {
            total: data_items.length,
            items: result,
        };
    },
});

let data_items = [
    { id: 1, name: 'name' },
    { id: 2, name: 'name' },
    { id: 3, name: 'name' },
    { id: 4, name: 'name' },
    { id: 5, name: 'name' },
    { id: 6, name: 'name' },
    { id: 7, name: 'name' },
    { id: 8, name: 'name' },
    { id: 9, name: 'name' },
    { id: 10, name: 'name' },
    { id: 11, name: 'name' },
    { id: 12, name: 'name' },
    { id: 13, name: 'name' },
    { id: 14, name: 'name' },
    { id: 15, name: 'name' },
    { id: 16, name: 'name' },
    { id: 17, name: 'name' },
    { id: 18, name: 'name' },
    { id: 19, name: 'name' },
    { id: 20, name: 'name' },
];
