let { Api, Joi, Util } = require('../../../../../index');
let Path = require('path');

module.exports = Api.defineEndpoint({
    method: 'post',
    path: '/upload',
    response: Joi.array().items(
        Joi.object({
            link: Joi.string(),
            file_name: Joi.string(),
            mime_type: Joi.string(),
            size: Joi.number(),
        })
    ),
    middlewares: [
        {
            name: 'file_upload',
            enable: true,
            option: {
                field: 'files',
                mode: 'array',
                temp_dir: 'upload',
                max_file_size: 3000000,
                file_name_handler: ({ file }) => {
                    return (
                        Util.randomUuid({ include_separator: false }) +
                        Path.extname(file.originalname)
                    );
                },
            },
        },
    ],
    handler: async function ({ files, service, _module }) {
        // #region [chuẩn bị dữ liệu phản hồi]

        let result = [];
        for (let file of files) {
            let item = {
                file_path: file.path,
                file_name: file.filename,
                dir_path: file.destination,
                mime_type: file.mimetype,
                size: file.size,
            };

            item.link =
                'http://localhost:' +
                service.api_server.option.port +
                '/file/' +
                item.file_name;
            result.push(item);
        }

        // #endregion

        return result;
    },
});
