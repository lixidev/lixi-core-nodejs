let { Api, Joi } = require('../../../../../index');

module.exports = Api.defineEndpoint({
    method: 'post',
    path: '/calculator',
    parameter: {
        a: Joi.number().allow(null).invalid(0),
        b: Joi.number()
            .allow(null)
            .when('a', { then: Joi.invalid(0) }),
    },
    response: Joi.object({
        a: Joi.number(),
        b: Joi.number(),
        is_a_undefined: Joi.boolean(),
        is_b_undefined: Joi.boolean(),
        sum: Joi.number(),
        sub: Joi.number(),
        mul: Joi.number(),
        div: Joi.number(),
        error: Joi.string(),
    }),
    handler: async function ({ args, _module }) {
        return await _module.func.runFunc({
            type: 'biz',
            name: 'handler',
            args,
        });
    },
});
