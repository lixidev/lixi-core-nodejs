let { Func, Joi } = require('../../../../../index');

module.exports = Func.defineFunc({
    name: 'handler',
    parameter: {
        a: Joi.number().allow(null).invalid(0),
        b: Joi.number()
            .allow(null)
            .when('a', { then: Joi.invalid(0) }),
    },
    response: Joi.object({
        a: Joi.number(),
        b: Joi.number(),
        is_a_undefined: Joi.boolean(),
        is_b_undefined: Joi.boolean(),
        sum: Joi.number(),
        sub: Joi.number(),
        mul: Joi.number(),
        div: Joi.number(),
        error: Joi.string(),
    }),
    handler: async function ({ args: { a, b } }) {
        let result = {
            a,
            b,
            is_a_undefined: a === undefined,
            is_b_undefined: b === undefined,
        };

        if (![undefined, null].includes(a) && ![undefined, null].includes(b)) {
            result.sum = a + b;
            result.sub = a - b;
            result.mul = a * b;
            result.div = a / b;
        }

        return result;
    },
});
