let { Api, Joi } = require('../../../../../index');

module.exports = Api.defineEndpoint({
    method: 'get',
    path: '/hello',
    parameter: {
        names: Joi.array().items(Joi.string()).required(),
    },
    response: Joi.object({
        text: Joi.string(),
    }),
    handler: async function ({ args: { names } }) {
        return { text: `hello ${names.join(', ')}` };
    },
});
