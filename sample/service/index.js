let { Service, Api, Document } = require('../../index');
let Path = require('path');

module.exports = async function ({
    show_log_on_start = false,
    show_log_on_error = false,
} = {}) {
    // khởi tạo api server
    let api_server = await Api.createServer({
        list_version: ['1.0.0'],
        default_version: '1.0.0',
    });

    // khởi tạo service
    let service = Service.init({
        root_dir_path: __dirname,
        show_log_on_start,
        show_log_on_error,
        api_server,
        document: Document.init(),
    });

    // nạp các module
    await service.loadModuleDir({
        dir_path: Path.join(__dirname, './module'),
    });

    return service;
};
