/** tính giá trị của parameter */

let Joi = require('@hapi/joi')

module.exports = async function ({ target, field = 'parameter', input }) {
    let model = target[field]
    if (typeof model === 'function') {
        model = Joi.attempt(
            await model(input),
            Joi.object().unknown(true).required()
        )
    }

    if (model && Object.keys(model).length > 0) {
        model = Joi.object(model)
        model = model.default(model.validate().value)
    } else model = undefined

    target[field] = model
    return target[field]
}
