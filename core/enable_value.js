/** tính giá trị của enable */

module.exports = async function ({ target, field = 'enable', input }) {
    if (typeof target[field] === 'function') {
        target[field] = await target[field](input)
    }

    return target[field]
}
