/** đặt default cho response */

module.exports = function setDefault({ response }) {
    if (response.type === 'object') {
        for (let item of response.$_terms.keys) {
            item.schema = item.schema.allow(null)
            if (
                !item.schema._flags ||
                item.schema._flags.default === undefined
            ) {
                item.schema = item.schema.default(null)
            }

            item.schema = setDefault({
                response: item.schema,
            })
        }
    } else if (response.type === 'array') {
        for (let i = 0; i < response.$_terms.items.length; i++) {
            response.$_terms.items[i] = setDefault({
                response: response.$_terms.items[i],
            })
        }
    }

    response = response.allow(null)
    if (!response._flags || response._flags.default === undefined) {
        response = response.default(null)
    }

    return response
}
