/** đăng ký method cho instance */

let Joi = require('@hapi/joi')

module.exports = function ({
    key,
    description,
    deprecated,
    parameter,
    handler,
}) {
    if (parameter) {
        parameter = parameter(Joi)
        try {
            parameter = parameter.default(parameter.validate().value)
        } catch (e) {}
    }

    return { type: 'method', key, description, deprecated, parameter, handler }
}
