/** tạo instance */

let installer = require('./installer/_')

module.exports = function ({ description, initial, extensions }) {
    let result = initial || {}
    if (!extensions) return result

    for (let extension of extensions) {
        if (!installer.hasOwnProperty(extension.type)) {
            throw Error(`installer of "${extension.type}" is not support`)
        }

        if (
            extension.hasOwnProperty('key') &&
            result.hasOwnProperty(extension.key)
        ) {
            throw Error(
                `${extension.type} "${extension.key}" is already in used`
            )
        }

        result = installer[extension.type]({ result, extension })
    }

    return result
}
