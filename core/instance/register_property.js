/** đăng ký property cho instance */

module.exports = function ({ key, description, deprecated, get, set }) {
    return { type: 'property', key, description, deprecated, get, set }
}
