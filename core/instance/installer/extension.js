module.exports = function ({ result, extension }) {
    if (extension.key) {
        result[extension.key] = extension.extension.bind(result)()
    } else {
        let response = extension.extension.bind(result)()
        if (response !== undefined) result = response
    }

    return result
}
