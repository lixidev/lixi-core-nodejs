module.exports = {
    extension: require('./extension'),
    field: require('./field'),
    method: require('./method'),
    property: require('./property'),
}
