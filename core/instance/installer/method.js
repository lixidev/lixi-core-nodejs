let Joi = require('@hapi/joi')
let Callsite = require('callsite')

module.exports = function ({ result, extension }) {
    let { key, deprecated, parameter, handler } = extension

    let deprecated_message
    if (deprecated) {
        deprecated_message = `method "${key}" is deprecated`
        if (typeof deprecated === 'string') {
            deprecated_message += ', ' + deprecated
        }
    }

    if (deprecated_message && parameter) {
        result[key] = methodHandlerDeprecatedParameter({
            result,
            deprecated_message,
            parameter,
            handler,
        })
    } else if (deprecated_message && !parameter) {
        result[key] = methodHandlerDeprecated({
            result,
            deprecated_message,
            handler,
        })
    } else if (!deprecated_message && parameter) {
        result[key] = methodHandlerParameter({
            result,
            parameter,
            handler,
        })
    } else {
        result[key] = methodHandler({
            result,
            handler,
        })
    }

    return result
}

let methodHandlerDeprecatedParameter = ({
    result,
    deprecated_message,
    parameter,
    handler,
}) =>
    function (args) {
        console.warn(
            '\x1b[33m[WARNING] (%s): %s\x1b[0m',
            Callsite()[1].getFileName(),
            deprecated_message
        )

        args = Joi.attempt(args, parameter, {
            stripUnknown: true,
        })

        return handler.bind(result)(args)
    }

let methodHandlerDeprecated = ({ result, deprecated_message, handler }) =>
    function (args) {
        console.warn(
            '\x1b[33m[WARNING] (%s): %s\x1b[0m',
            Callsite()[1].getFileName(),
            deprecated_message
        )

        return handler.bind(result)(args)
    }

let methodHandlerParameter = ({ result, parameter, handler }) =>
    function (args) {
        args = Joi.attempt(args, parameter, {
            stripUnknown: true,
        })

        return handler.bind(result)(args)
    }

let methodHandler = ({ result, handler }) =>
    function (args) {
        return handler.bind(result)(args)
    }
