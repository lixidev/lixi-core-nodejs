module.exports = function ({ result, extension }) {
    let { key, field } = extension

    result[key] = field

    return result
}
