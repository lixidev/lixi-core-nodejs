let Callsite = require('callsite')

module.exports = function ({ result, extension }) {
    let { key, deprecated, get, set } = extension
    let property = {}

    let deprecated_message
    if (deprecated) {
        deprecated_message = `property "${key}" is deprecated`
        if (typeof deprecated === 'string') {
            deprecated_message += ', ' + deprecated
        }
    }

    if (get) {
        if (deprecated_message) {
            property.get = function () {
                console.warn(
                    '\x1b[33m[WARNING] (%s): %s\x1b[0m',
                    Callsite()[1].getFileName(),
                    deprecated_message
                )

                return get.bind(result)()
            }
        } else property.get = get.bind(result)
    }

    if (set) {
        if (deprecated_message) {
            property.set = function (value) {
                console.warn(
                    '\x1b[33m[WARNING] (%s): %s\x1b[0m',
                    Callsite()[1].getFileName(),
                    deprecated_message
                )

                return set.bind(result)(value)
            }
        } else property.set = set.bind(result)
    }

    Object.defineProperty(result, key, property)
    return result
}
