module.exports = {
    create: require('./create'),
    registerExtension: require('./register_extension'),
    registerField: require('./register_field'),
    registerMethod: require('./register_method'),
    registerProperty: require('./register_property'),
}
