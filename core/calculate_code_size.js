/** tính toán code size của file */

let Fs = require('fs')

module.exports = function ({ file_path }) {
    return Fs.readFileSync(file_path, 'utf8')
        .split('\n')
        .filter((line) => line.length > 0).length
}
