/** kiểm tra phần tử có tồn tại trong danh sách */

module.exports = function ({
    item,
    list,
    error_message,
    key_field = 'key',
    name_field = 'name',
}) {
    let exists_items = list.filter((x) => x[key_field] === item[key_field])
    if (exists_items.length === 0) return

    let message = error_message ? error_message + ', ' : ''
    message +=
        item[name_field] +
        ' is duplicated with ' +
        list.map((x) => x[name_field]).join(', ')

    throw Error(message)
}
