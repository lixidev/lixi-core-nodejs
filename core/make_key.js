/** tạo key từ chuỗi */

module.exports = function ({ text }) {
    text = text.trim().split(' ')

    for (let i = 0; i < text.length; i++) {
        text[i] = text[i].trim().toLowerCase()
    }

    return text.filter((x) => x.length > 0).join('_')
}
