/** tính giá trị của response */

let Joi = require('@hapi/joi')

module.exports = async function ({ target, field = 'response', input }) {
    let model = target[field]
    if (typeof model === 'function') {
        model = Joi.attempt(
            await model(input),
            Joi.object().schema().required()
        )
    }

    if (model) {
        model = model.required().label('response')
        model = setDefault({ model })
    }

    target[field] = model
    return target[field]
}

function setDefault({ model }) {
    if (model.type === 'object') {
        for (let item of model.$_terms.keys) {
            item.schema = item.schema.allow(null)
            if (
                !item.schema._flags ||
                item.schema._flags.default === undefined
            ) {
                item.schema = item.schema.default(null)
            }

            item.schema = setDefault({
                model: item.schema,
            })
        }
    } else if (model.type === 'array') {
        for (let i = 0; i < model.$_terms.items.length; i++) {
            model.$_terms.items[i] = setDefault({
                model: model.$_terms.items[i],
            })
        }
    }

    model = model.allow(null)
    if (!model._flags || model._flags.default === undefined) {
        model = model.default(null)
    }

    return model
}
