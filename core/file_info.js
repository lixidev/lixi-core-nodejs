/** lấy thông tin file */

let Callsite = require('callsite')
let Path = require('path')

module.exports = function ({ callsite_index = 3 } = {}) {
    let file_name = Callsite()[callsite_index].getFileName()
    return {
        name: Path.basename(file_name, Path.extname(file_name)),
        path: file_name,
    }
}
