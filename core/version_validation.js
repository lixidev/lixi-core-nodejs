let Semver = require('semver')

module.exports = function (value) {
    if (!Semver.valid(value)) {
        throw Error(`"${value}" is invalid with semantic versioning`)
    }

    return value
}
