/** lấy danh sách file từ dường dẫn chỉ định */

let Path = require('path')
let Fs = require('fs')

module.exports = function listAllFile({ path, extnames, _result }) {
    if (!_result) _result = []
    if (!Fs.existsSync(path)) return _result

    let items = Fs.readdirSync(path)
    for (let name of items) {
        let item_path = Path.join(path, name)

        if (Fs.statSync(item_path).isDirectory()) {
            _result = listAllFile({ path: item_path, extnames, _result })
        } else {
            if (extnames && !extnames.includes(Path.extname(name))) {
                continue
            }

            _result.push(item_path)
        }
    }

    return _result
}
