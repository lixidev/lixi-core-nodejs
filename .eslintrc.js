module.exports = {
    extends: ['google', 'prettier'],
    plugins: ['prettier'],
    env: {
        commonjs: true,
        es6: true,
        node: true,
    },
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        'camelcase': 'off',
        'new-cap': 'off',
        'no-undef': 'error',
        'no-unused-vars': 'warn',
        'no-var': 'error',
        'one-var': 'off',
        'prefer-const': 'off',
        'require-jsdoc': 'off',
    },
}
